<?php
/*
 * Template Name: Projects landing page
 */
	// load header
	get_header();
	///echo do_shortcode('[build-project-category-menu]');
?>
	
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<div class="row-fluid">
			<div class="span12">
		<?php
		// load featured image
			if ( has_post_thumbnail() ) {
			$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail());
			echo  '<div class="featured_image"><span class="projects_cluster_large_img">'.$img_string.'</span></div>';
			} 
		?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
				<section>	
				<header class="entry-header">
					<h2 class="entry-title"><?php the_title(); ?></h2>
				</header><!-- .entry-header -->
				<div class="projects_back_btn" style=" float: right; width: 180px;">
					<a class="btn btn-info" href="/project-locations" title="Map of all projects">Projects Map &#187</a>
				</div>
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">							
							<?php the_content(); ?>
						</div><!-- .entry-content -->
				</article><!-- #post -->
			<?php endwhile; ?>
				</section>
				<section id="downloads">
					<?php
						if (function_exists('userTypeAttachments')) {
							userTypeAttachments();
						}
					?>
				</section>
			
				<section id="news">
					
					<?php 
	global $wpdb;
	
	$default_project_array = array('Contractor','Developer', 'Refurbishment');
	
	if(isset($_REQUEST['category'])):
		if($_REQUEST['category'] == 'all'):			
			$project_array = $default_project_array;
		else:
			$project_array = array($_REQUEST['category']);
		endif;
	else:
		$project_array = $default_project_array;
	endif;	
	
	$project_array_values = "'" . implode("','", $project_array) . "'";
						
$sql = $wpdb->get_results("SELECT t1.ID, t1.post_title, t1.post_excerpt, t1.post_name, t2.term_taxonomy_id, t3.name, t3.slug  FROM `{$wpdb->prefix}posts` AS t1 INNER JOIN `{$wpdb->prefix}term_relationships` AS t2 ON t2.object_id IN(t1.ID) INNER JOIN `{$wpdb->prefix}terms` AS t3 ON t2.term_taxonomy_id IN(t3.term_id)  WHERE t1.post_type = 'projects_ct' AND t1.post_status = 'publish' AND t3.slug IN ($project_array_values) ORDER BY t3.name ASC");


$tag_sql = $wpdb->get_results("SELECT name, slug FROM `{$wpdb->prefix}terms` ORDER BY name ASC");

/* INDEX of array order
	All = 1
	Social housing new build = 24
	Private residential = 20
	Mixed-use = 13
	PPP = 19
	Regeneration = 23
	Care homes = 3
	Key worker = 10
	High-rise = 9
	Refurbishment = 21
	Low-carbon retrofit = 11
	WHISCERS = 26
*/
$array_order_by = array(1, 24, 20, 13, 19, 23, 3, 10, 9, 21, 11, 26);

if(is_array($tag_sql)):
	$not_in_tag_array = array('Uncategorized','post-format-status','Active','Blog','Completed','Contractor','Events','Future','Menu1','post-format-audio','post-format-gallery','post-format-link','Developer','News', 'Refurbishments');
	

	echo '<nav class="projects_navigation">
			<form id="yourForm" action="/projects" method="post">';
	$i = 'null';
	foreach($array_order_by as $index):
		$i = $i + 1;
		
		
		if(!in_array($tag_sql[$index]->name , $not_in_tag_array)):
			
			if(isset($_REQUEST['category'])):
				$set_post_value = $_REQUEST['category'];
			else:
				$set_post_value = 'all';			
			endif;	
			
				if($set_post_value == $tag_sql[$index]->slug):
					$html_output = '<li><input class="t1" type="radio" name="category" value="'.$tag_sql[$index]->slug.'" checked /><span>'.$tag_sql[$index]->name.'</span></li>';
				else:
					$html_output = '<li><input class="t1" type="radio" name="category" value="'.$tag_sql[$index]->slug.'" /><span>'.$tag_sql[$index]->name.'</span></li>';
				endif;	
				
				if ($i == 1):
					// group open
					echo '<ul class="nav nav-pills">';
				elseif ($i == 7):
					echo '<ul class="nav nav-pills">';
				endif;
				echo $html_output;	
				if ($i == 6 || $i == 12):
					// group open
					echo '</ul>';
				endif;
			
		endif;
	endforeach;
	/*foreach($tag_sql as $tag):
		if(!in_array($tag->name , $not_in_tag_array)):
			
			if(isset($_REQUEST['category'])):
				$set_post_value = $_REQUEST['category'];
			else:
				$set_post_value = 'all';			
			endif;	
			
				if($set_post_value == $tag->slug):
					$html_output = '<li><input class="t1" type="radio" name="category" value="'.$tag->slug.'" checked /><span>'.$tag->name.'</span></li>';
				else:
					$html_output = '<li><input class="t1" type="radio" name="category" value="'.$tag->slug.'" /><span>'.$tag->name.'</span></li>';
				endif;	
				
				echo $html_output;	
			
		endif;
	endforeach;*/
	echo '</from></nav>';
endif;

if (class_exists('pagination')) {

	$tax_name_one = array();
	$tax_name_two = array();
	$advanced_paginations = new pagination;
	
	$limit = get_option('posts_per_page');
	$items = $advanced_paginations->generate($sql, $limit);
	
	echo '<div class="pagination pagination-left">'.$advanced_paginations->links().'</div>';
	
	foreach ($items as $dataKey):
	
		//$meta_ref = get_post_meta($dataKey->ID, $meta_type, true);
		
		if(!in_array($dataKey->name, $tax_name_one, true)){
			array_push($tax_name_one, $dataKey->name);
			echo '<section>';
			//echo '<h2 class="section_header_'.strtolower($dataKey->name).'"><span></span>'.$dataKey->name.'</h2>';	
		}
		
		
		
		echo '<article class="newsItems">';
		if(get_the_post_thumbnail($dataKey->ID) != ''):
			
			
			$news_img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($dataKey->ID, 'latest-news-list-thumb'));
			
			
			echo  '<span class="projects_cluster_large_img ie_news_image">'.get_the_post_thumbnail($dataKey->ID, 'latest-news-list-thumb').'</span>';
		endif;	
		
		$excerpt = $dataKey->post_excerpt;		
		$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
		$excerpt = strip_shortcodes($excerpt);
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, 280);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));		
				
		echo '<header><h2 class="section_header_'.strtolower($dataKey->name).'"><span></span><a href="/projects_ct/'.$dataKey->post_name.'" title="'.$dataKey->post_title.'">'.$dataKey->post_title.'</a></h2></header>';
		echo '<p>'.$excerpt.'... <br/><a href="/projects_ct/'.$dataKey->post_name.'" title="'.$dataKey->post_title.'"><strong>Read more...</strong></a></p>';
		echo '</article>';
		
		
			
		if(!in_array($dataKey->name, $tax_name_two, true)){
			array_push($tax_name_two, $dataKey->name);
			echo '</section>';
		}
							
	endforeach;
}
	echo '<hr class="pagination_clearfix"  />';
	echo '<div class="pagination pagination-left">'.$advanced_paginations->links().'</div>';
?>
				</section>
			</div>
		</div>		
	</div>
</div>

<?php
	
	
	// load footer
	get_footer();
	
?>

