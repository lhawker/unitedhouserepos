<?php
/*
 * Template Name: News landing page
 */
	// load header
	get_header();
	///echo do_shortcode('[build-project-category-menu]');
?>
	
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<div class="row-fluid">
			<div class="span12">
		<?php
		// load featured image
			if ( has_post_thumbnail() ) {
			/*$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail());*/
			$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($post->ID, 'large'));
			
			echo  '<div class="featured_image"><span class="projects_cluster_large_img">'.$img_string.'</span></div>';
			} 
		?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
				<section>	
				<header class="entry-header">
					<h2 class="entry-title"><?php the_title(); ?></h2>
				</header><!-- .entry-header -->
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content(); ?>
							<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</div><!-- .entry-content -->
				</article><!-- #post -->
			<?php endwhile; ?>
				</section>
				<section id="downloads">
					<?php
						if (function_exists('userTypeAttachments')) {
							userTypeAttachments();
						}
					?>
				</section>
				<section id="news">
					<?php
						if(class_exists('AllNewsItems')):
						$ListAllNews = new AllNewsItems;
						$ListAllNews->AllNews();
						endif;
					?>
				</section>
			</div>
		</div>		
	</div>
</div>

<?php
	
	
	// load footer
	get_footer();
	
?>

