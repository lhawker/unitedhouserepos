<?php
class bootstrap_breadcrumbs {
	
	public function get_breadcrumbs($class){
		
		global $post;
		
		$showOnHome 	= 1; 			// 1 - show breadcrumbs on the homepage, 0 - don't show
		$delimiter		= '<span class="divider">&raquo;</span>'; 	// delimiter between crumbs
		$home 			= 'Home'; 		// text for the 'Home' link
		$showCurrent 	= 1; 			// 1 - show current post/page title in breadcrumbs, 0 - don't show
		$before 		= '<li>'; // tag before the current crumb
		$before_active 		= '<li class="active">'; // tag before the current crumb
		$after 			= '</li>'; 	// tag after the current crumb
		$homeLink = get_bloginfo('url');
	

		if (is_home() || is_front_page()){
			if ($showOnHome == 1) echo '<ul class="'. $class .'"><li class="active"><a href="' . $homeLink . '">' . $home . '</a></li></ul>';
		}else{
			echo '<ul class="'. $class .'"><li><a href="' . $homeLink . '">' . $home . '</a>' . $delimiter . ' </li>';

		if (is_category()){
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
			echo $before_active . 'Archive by category "' . single_cat_title('', false) . '"' . $after;

		}else
		if (is_search()){
			echo $before_active . 'Search results for "' . get_search_query() . '"' . $after;
		}else
		if (is_day()){
			echo $before.'<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' '.$after;
			echo $before.'<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' '.$after;
			echo $before_active . get_the_time('d') . $after;

		}else
		if(is_month()){
			echo $before.'<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' '.$after;
			echo $before_active . get_the_time('F') . $after;
		}else
		if(is_year()){
			echo $before_active . get_the_time('Y') . $after;
		}else
		if (is_single() && !is_attachment()){
			if (get_post_type() != 'post'){
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo $before.'<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>'.$after;
				if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before_active . get_the_title() . $after;
			}else{
			$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
				if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
				echo $cats;
				if ($showCurrent == 1) echo ''.$before_active . get_the_title() . $after;
			}
		}else
		if(!is_single() && !is_page() && get_post_type() != 'post' && !is_404()){
			$post_type = get_post_type_object(get_post_type());
			echo $before_active . $post_type->labels->singular_name . $after;
		}else
		if (is_attachment()){
			/*$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo $before.'<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>'.$after;
			if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before_active . get_the_title() . $after;*/
		}else
		if (is_page() && !$post->post_parent){
			if ($showCurrent == 1) echo $before_active . get_the_title() . $after;
		}else
		if (is_page() && $post->post_parent){
			$parent_id = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = $before.'<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>'.$after;
				$parent_id = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo $breadcrumbs[$i];
				if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
			}
			if ($showCurrent == 1) echo $before_active . ' '. $delimiter .get_the_title() . $after;

		}else
		if (is_tag()){
			echo $before_active . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
		}else
		if (is_author()){
			global $author;
			$userdata = get_userdata($author);
			echo $before_active . 'Articles posted by ' . $userdata->display_name . $after;
		}else
		if (is_404()){
			echo $before_active . 'Error 404' . $after;
		}

		if (get_query_var('paged')){
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
			echo __('Page') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}
		echo '</ul>';
	}
	}

}
?>