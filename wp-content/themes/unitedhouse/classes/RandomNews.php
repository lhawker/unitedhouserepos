<?php
class RandomNews {
	public function getRandomNews(){
		global $wpdb;
		
		$sql = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}posts` WHERE post_type= 'post' AND post_status = 'publish' ORDER BY `post_date`  LIMIT 6 ");	
		$num = 0;
	
	if(!empty($sql)):
		$num = count($sql) - 1;
		$randomSelected = rand(0, $num);
		
		$excerpt = $sql[$randomSelected]->post_content;
		$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
		$excerpt = strip_tags($excerpt);
		$excerpt = substr($excerpt, 0, 320);
		$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
		$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));							
		
		if(get_the_post_thumbnail($sql[$randomSelected]->ID) != ''):
			$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($sql[$randomSelected]->ID, 'thumbnail'));
			echo  '<div class="landing_page_news_img">'.$img_string.'</div>';
		endif;		
		
		echo '<h2 class="sub_heading"><a href="/'.$sql[$randomSelected]->post_name.'" title="'.$sql[$randomSelected]->post_title.'">'.$sql[$randomSelected]->post_title.'</a></h2>';		
		echo $excerpt.' ...';
		echo '<p><a class="btn btn-info" href="/'.$sql[$randomSelected]->post_name.'" title="'.$sql[$randomSelected]->post_title.'">Read more</a></p>';	
	endif;

	}
	
	public function getLatestNews($limit_num){
		global $wpdb;
		$sql = $wpdb->get_results("SELECT ID, post_title, post_excerpt, post_name, guid FROM `{$wpdb->prefix}posts` WHERE post_type= 'post' AND post_status = 'publish' ORDER BY `post_date`  LIMIT $limit_num ");	
		return $sql;
	}
}
?>