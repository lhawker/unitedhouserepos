<?php

class ListAllProjectsTaxonomys {
	
	public function getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id) {		
			
		if(isset($current_taxonomy) && isset($tag_tax_type) && isset($project_tag_status) && isset($current_taxonomy_id)):
			global $wpdb;
			
			$sql = $wpdb->get_results("SELECT DISTINCT
											p1.ID AS post_ID,
											p1.post_title AS post_title,
											p1.post_name AS post_name,
											p1.post_excerpt AS post_excerpt,
											p1.guid AS post_guid,
											terms2.term_id AS tag_ID,
											terms2.name AS tag_name,
											t2.count AS posts_with_tag
										FROM 
										soc_posts AS p1
										LEFT JOIN `{$wpdb->prefix}term_relationships` AS r1 ON p1.ID = r1.object_ID
										LEFT JOIN `{$wpdb->prefix}term_taxonomy` AS t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
										LEFT JOIN `{$wpdb->prefix}terms` AS terms1 ON t1.term_id = terms1.term_id,
										soc_posts AS p2
										LEFT JOIN `{$wpdb->prefix}term_relationships` AS r2 ON p2.ID = r2.object_ID
										LEFT JOIN `{$wpdb->prefix}term_taxonomy` AS t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
										LEFT JOIN `{$wpdb->prefix}terms` AS terms2 ON t2.term_id = terms2.term_id
										WHERE (
											t1.taxonomy = '$current_taxonomy' AND
											p1.post_status = 'publish' AND
											terms1.term_id = '$current_taxonomy_id' AND
											t2.taxonomy = '$tag_tax_type' AND
											p2.post_status = 'publish' AND
											p1.ID = p2.ID AND
											terms2.name = '$project_tag_status'
										)");			
			
			echo '<section class="project_tagged_clusters">';
				if(!empty($sql)):
					echo '<div class="carousel slide" id="Carousel_'.$project_tag_status.'">
					<div class="carousel-inner">';
				$i = 0;
				foreach ($sql as $project):
					$i = $i + 1;
					if($i > 1):
						$active_css_class = '';
					else:
						$active_css_class = 'active';
					endif;
						
					echo '<!-- loop here -->
              			<div class="item span12 '.$active_css_class.'">
                    		<ul class="thumbnails">
                        		<li class="span5">
                            		<div>';
									if(get_the_post_thumbnail($project->post_ID) != ''):
										$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($project->post_ID, 'project-carousel-image'));
										echo  '<span class="projects_cluster_large_img">'.$img_string.'</span>';
										endif;
										
					
					$excerpt = $project->post_excerpt;		
					$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
					$excerpt = strip_shortcodes($excerpt);
					$excerpt = strip_tags($excerpt);
					$excerpt = substr($excerpt, 0, 220);
					$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
					$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));		
			
			
                     echo '</div>
                        		</li>
								<li class="span7">
                            		<h2>'.$project->post_title.'</h2>
									<p>'.$excerpt.'...</p>
									<p><a href="/projects_ct/'.$project->post_name.'" title="">Read more...</a></p>
                        		</li>
                    		</ul>
              			</div>
						<!-- end loop here -->';
				endforeach;

			echo '</div>';
			
			if(count($sql) > 1):
            	echo'<a data-slide="prev" href="#Carousel_'.$project_tag_status.'" class="left carousel-control">‹</a>
            		<a data-slide="next" href="#Carousel_'.$project_tag_status.'" class="right carousel-control">›</a>';
			endif;
			
        	echo '</div>';
				else:
					echo '<h2>Sorry, but there are no "'.$project_tag_status.'" projects at this time!</h2>';
				endif;
			echo '</section>';
			
		endif;
		
	}
		
	
	public function getAllProjectsTaxonomys($current_taxonomy, $current_taxonomy_id, $ajax_link_selector, $ajax_display_selector) {
		if(isset($current_taxonomy)):
			global $wpdb;
			
			/*$sql = $wpdb->get_results("SELECT t1.term_taxonomy_id, t2.name, t2.slug  FROM `{$wpdb->prefix}term_taxonomy` AS t1 INNER JOIN `{$wpdb->prefix}terms` AS t2 ON t1.term_taxonomy_id IN(t2.term_id) WHERE t1.taxonomy = '$current_taxonomy' ORDER BY t2.name");	*/
			
			
			$sql = $wpdb->get_results("SELECT t1.term_taxonomy_id, t2.name, t2.slug  FROM `{$wpdb->prefix}term_taxonomy` AS t1 INNER JOIN `{$wpdb->prefix}terms` AS t2 ON t1.term_taxonomy_id IN(t2.term_id) WHERE t1.taxonomy = 'projects_tags' ORDER BY t2.name");

	
			if(!empty($sql)):
				// render jq ajax funxtions
				echo '<script type="text/javascript">
						$ = jQuery;
						jQuery(document).ajaxStart(function() {
   							jQuery("#loadingDiv").show();
 						});
						jQuery(document).ajaxStop(function() {
   							jQuery("#loadingDiv").hide();
 						});

						jQuery(document).ready(function(){
							
							jQuery("#loadingDiv").hide();
							
							jQuery("'.$ajax_link_selector.' li a").click(function () {
        					jQuery("'.$ajax_display_selector.'").load(jQuery(this).attr("name") + "#ajaxContent");

							//type-projects
    						//console.log(jQuery(this).attr("name"));
    						});

							jQuery("#showAllProjects").click(function () {
        					jQuery("'.$ajax_display_selector.'").load(jQuery(this).attr("name") + "#ajaxContent");

							//type-projects
    						//console.log(jQuery(this).attr("name"));
    						});
						
						});
						</script>';
						echo '<a id="showAllProjects" class="btn btn-danger" name="/projects/all?tax=ALL&amp;cat_id='.$current_taxonomy_id.'">
    								Show all Projects
  							  </a>&nbsp;';

				// render html output
				echo '<div class="btn-group">
  					<a class="btn btn-danger dropdown-toggle" data-toggle="dropdown" href="#">
    					Filter Projects by
    					<span class="caret"></span>
  					</a>
  					<ul id="tax_filter"class="dropdown-menu">
    					<!-- dropdown menu links -->';
				if(strtolower(get_the_title()) == 'developer'):
					$current_tax_id = 20;
					$allowed_tags_array = array('active', 'completed');
					$array_order_by = array(0,2);
					
				elseif(strtolower(get_the_title()) == 'contractor'):
					$current_tax_id = 21;
					$allowed_tags_array = array('social-housing-new-build','private-residential','ppp','mixed-use','regeneration','care-homes','key-worker','high-rise');
					$array_order_by = array(12,9,7,8,11,1,5,4);
				elseif(strtolower(get_the_title()) == 'refurbisher'):
					$current_tax_id = 2;
					$allowed_tags_array = array('refurbishments','low-carbon-retrofit','whiscers');
					$array_order_by = array(10,6,13);
				else:
					$current_tax_id = 0;
					$allowed_tags_array = array('active', 'completed','social-housing-new-build','private-residential','ppp','mixed-use','regeneration','care-homes','key-worker','high-rise', 'refurbishments','low-carbon-retrofit','whiscers');
					$array_order_by = array();
				endif;					
				
				//foreach($sql as $item):
				foreach($array_order_by as $index):
					if($sql[$index]->name != 'ALL'):
						//echo '<li><a name="/projects/'.$item->slug.'?tax='.$item->term_taxonomy_id.'">'.$item->name.'</a></li>';
						if(in_array($sql[$index]->slug, $allowed_tags_array)):	
						if($sql[$index]->name == 'Active' || $sql[$index]->name == 'Completed'):
							$input_title = 'Projects';
						else:
							$input_title = '';
						endif;			
						echo '<li><a name="/projects_tags/'.$sql[$index]->slug.'?tax='.$sql[$index]->term_taxonomy_id.'&cat='.$current_tax_id.'">'.$sql[$index]->name.' '.$input_title.'</a></li>';
						endif;
						
					endif;					
				endforeach;
				echo '</ul></div>';
			endif;
		endif;
	}
	
	public function listProjectsByQuery($queryString, $limit, $defaultProjects) {

		if(isset($queryString)):
			global $wpdb;
			if($queryString == 'ALL'){
				
				if(isset($_REQUEST["cat_id"])):
					$cat_id = $_REQUEST["cat_id"];
				else:
					$cat_id = '';
				endif;
				
				$sql = $wpdb->get_results("SELECT ID, post_name, post_date, post_title, post_excerpt, guid, meta_value AS start_date FROM `{$wpdb->prefix}term_relationships` INNER JOIN `{$wpdb->prefix}posts` ON object_id IN(ID) INNER JOIN `{$wpdb->prefix}postmeta` ON object_id IN(post_id) WHERE term_taxonomy_id = '$cat_id' AND post_status = 'publish' AND meta_key = 'start_date' ORDER BY start_date");	

			} elseif($defaultProjects =='DEFAULT_PROJECTS') {
								
				$sql = $wpdb->get_results("SELECT ID, post_name, post_date, post_title, post_excerpt, guid, meta_value AS start_date FROM `{$wpdb->prefix}term_relationships` INNER JOIN `{$wpdb->prefix}posts` ON object_id IN(ID) INNER JOIN `{$wpdb->prefix}postmeta` ON object_id IN(post_id) WHERE term_taxonomy_id = '$queryString' AND post_status = 'publish' AND meta_key = 'start_date' ORDER BY start_date LIMIT $limit");		
				
					
			} else {
				$sql = $wpdb->get_results("SELECT ID, post_name, post_date, post_title, post_excerpt, guid, meta_value AS start_date FROM `{$wpdb->prefix}term_relationships` INNER JOIN `{$wpdb->prefix}posts` ON object_id IN(ID) INNER JOIN `{$wpdb->prefix}postmeta` ON object_id IN(post_id) WHERE term_taxonomy_id = '$queryString' AND post_status = 'publish' AND meta_key = 'start_date' ORDER BY start_date");
			}
					
			return $sql;
		endif;
		 
	}
	
	
	public function listProjectsByQueryForTags($queryString, $cat_id, $defaultProjects) {
		$limit = 5;

		if(isset($queryString)):
			global $wpdb;
			if($queryString == 'ALL'){
				
				if(isset($_REQUEST["cat_id"])):
					$cat_id = $_REQUEST["cat_id"];
				else:
					$cat_id = '';
				endif;
				
				$sql = $wpdb->get_results("SELECT ID, post_name, post_date, post_title, post_excerpt, guid, meta_value AS start_date FROM `{$wpdb->prefix}term_relationships` INNER JOIN `{$wpdb->prefix}posts` ON object_id IN(ID) INNER JOIN `{$wpdb->prefix}postmeta` ON object_id IN(post_id) WHERE term_taxonomy_id = '$cat_id' AND post_status = 'publish' AND meta_key = 'start_date' ORDER BY start_date");	

			} elseif($defaultProjects =='DEFAULT_PROJECTS') {
								
				$sql = $wpdb->get_results("SELECT ID, post_name, post_date, post_title, post_excerpt, guid, meta_value AS start_date FROM `{$wpdb->prefix}term_relationships` INNER JOIN `{$wpdb->prefix}posts` ON object_id IN(ID) INNER JOIN `{$wpdb->prefix}postmeta` ON object_id IN(post_id) WHERE term_taxonomy_id = '$queryString' AND post_status = 'publish' AND meta_key = 'start_date' ORDER BY start_date LIMIT $limit");		
				
					
			} else {
				/*$sql = $wpdb->get_results("SELECT ID, post_name, post_date, post_title, post_excerpt, guid, meta_value AS start_date FROM `{$wpdb->prefix}term_relationships` INNER JOIN `{$wpdb->prefix}posts` ON object_id IN(ID) INNER JOIN `{$wpdb->prefix}postmeta` ON object_id IN(post_id) WHERE term_taxonomy_id = '$queryString' AND post_status = 'publish' AND meta_key = 'start_date' ORDER BY start_date");*/
			
				/***************** changed the query to look for all tags under current cat */
			

				$term_id = $queryString;
				$current_taxonomy = 'projects';
				$tag_tax_type = 'projects_tags';				
				$term = get_term_by('term_id', $term_id, $tag_tax_type);
				$project_tag_status = $term->name;
				$current_taxonomy_id = $cat_id;

				$sql = $wpdb->get_results("SELECT DISTINCT
											p1.ID AS post_ID,
											p1.post_title AS post_title,
											p1.post_name AS post_name,
											p1.post_excerpt AS post_excerpt,
											p1.guid AS post_guid,
											terms2.term_id AS tag_ID,
											terms2.name AS tag_name,
											t2.count AS posts_with_tag
										FROM 
										soc_posts AS p1
										LEFT JOIN `{$wpdb->prefix}term_relationships` AS r1 ON p1.ID = r1.object_ID
										LEFT JOIN `{$wpdb->prefix}term_taxonomy` AS t1 ON r1.term_taxonomy_id = t1.term_taxonomy_id
										LEFT JOIN `{$wpdb->prefix}terms` AS terms1 ON t1.term_id = terms1.term_id,
										soc_posts AS p2
										LEFT JOIN `{$wpdb->prefix}term_relationships` AS r2 ON p2.ID = r2.object_ID
										LEFT JOIN `{$wpdb->prefix}term_taxonomy` AS t2 ON r2.term_taxonomy_id = t2.term_taxonomy_id
										LEFT JOIN `{$wpdb->prefix}terms` AS terms2 ON t2.term_id = terms2.term_id
										WHERE (
											t1.taxonomy = '$current_taxonomy' AND
											p1.post_status = 'publish' AND
											terms1.term_id = '$current_taxonomy_id' AND
											t2.taxonomy = '$tag_tax_type' AND
											p2.post_status = 'publish' AND
											p1.ID = p2.ID AND
											terms2.name = '$project_tag_status'
										)");
											
				
				/********************  working above here */
				
		
			}
					
			return $sql;
		endif;
		 
	}
}
?>