<?php

// Notes to self:
/* Include this class into the functions file (require get_template_directory() . '/classes/FindMaps.php';)
 *
 * Create new class instance within the page template 
	<?php
		global $post;				
		$post_id = $post->ID;
		$post_type = $post->post_type;		
		$mapResults = new FindMaps();
		
		// call function to return all log-lat from the maps.
		$mapResults->getAllMaps($post_id, $post_type);	
	?>
 * Please note that I'll need to think about what post types i'm passing in to the class instance
   
 *
 *
 *
 */

class FindMaps {
	
	// get only map items from cat start
	public function  getCatMaps($post_id, $post_type, $post_tax, $post_title) {
		
		global $wpdb;
		$project_map_data_array = array();

		// check that the post id is set
		if(isset($post_id) || isset($post_type)):
			
			// sql
			$sql = $wpdb->get_results("SELECT t1.ID, t1.post_title, t1.post_excerpt, t1.post_name, t1.guid, t3.obj FROM (SELECT * FROM `{$wpdb->prefix}posts`) AS t1
			INNER JOIN `{$wpdb->prefix}mappress_posts` AS t2 ON t1.id IN (t2.postid) INNER JOIN `{$wpdb->prefix}mappress_maps` AS t3 ON t2.mapid IN (t3.mapid) WHERE post_type='$post_type' ORDER BY t1.post_title ASC");			
			
			// check for results
			if(!empty($sql)):
				// loop over results				
				foreach($sql as $obj):

					$mapdata_unserialize = (array) unserialize($obj->obj);								
					
					$category = get_the_terms( $obj->ID, $post_tax );
					
					// only run if array is not empty
					if(!empty($category)):
						$category_name = $category[current(array_keys($category))]->name;
					else:
						$category_name = 0;
					endif;
					
					// only run if array is not empty
					if(!empty($mapdata_unserialize)):					
						$map_obj = $mapdata_unserialize['pois'][0]->point['lat'];						
						$lat = $mapdata_unserialize['pois'][0]->point['lat'];						
						$lng = $mapdata_unserialize['pois'][0]->point['lng'];						
					else:
						$lng = 0;
						$lat = 0;
					endif;
					
					// Check, we don't want to add the item to the array if it does not have a $lat, $lng or $category_name					
					if(!$lng == 0 || !$lat == 0 || !$category_name == 0):
						
						if( $category_name == 'Developer'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/developer_map_icon.png';
						elseif ($category_name == 'Refurbishment'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/refurbishment_map_icon.png';
						elseif ($category_name == 'Contractor'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/contractor_map_icon.png';
						else:
							$marker_icon = 'http://maps.google.com/mapfiles/marker_green.png';
						endif;
						
						if($post_title == strtolower('refurbisher')):
							$post_title = 'refurbishment';
						endif;
						
						if($post_title == strtolower($category_name)):
							$project_map_data_array[] = array(
														  'latLng' => '['.$lat.', '.$lng.']', 
														  'data' => '\'<h2><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">'.$obj->post_title.'</a></h2><p>'.$obj->post_excerpt.'</p>\'', 
														  'options' => '{icon: \''.$marker_icon.'\'}'
														);	
						endif;
															
					endif;
				endforeach;	
					
			endif;	
			return str_replace('"', '', str_replace('\\/', '/', json_encode($project_map_data_array)) );		
		endif;	
	}
	
	// get only map items from cat end
		
	public function  getAllMaps($post_id, $post_type, $post_tax) {
		
		global $wpdb;
		$project_map_data_array = array();
		
		// check that the post id is set
		if(isset($post_id) || isset($post_type)):
			
			// sql
			$sql = $wpdb->get_results("SELECT t1.ID, t1.post_title, t1.post_excerpt, t1.post_name, t1.guid, t3.obj FROM (SELECT * FROM `{$wpdb->prefix}posts`) AS t1
			INNER JOIN `{$wpdb->prefix}mappress_posts` AS t2 ON t1.id IN (t2.postid) INNER JOIN `{$wpdb->prefix}mappress_maps` AS t3 ON t2.mapid IN (t3.mapid) WHERE post_type='$post_type' ORDER BY t1.post_title ASC");			
			
			// check for results
			if(!empty($sql)):
				// loop over results				
				foreach($sql as $obj):

					$mapdata_unserialize = (array) unserialize($obj->obj);								
					
					$category = get_the_terms( $obj->ID, $post_tax );
					
					// only run if array is not empty
					if(!empty($category)):
						$category_name = $category[current(array_keys($category))]->name;
					else:
						$category_name = 0;
					endif;
					
					// only run if array is not empty
					if(!empty($mapdata_unserialize)):					
						$map_obj = $mapdata_unserialize['pois'][0]->point['lat'];						
						$lat = $mapdata_unserialize['pois'][0]->point['lat'];						
						$lng = $mapdata_unserialize['pois'][0]->point['lng'];						
					else:
						$lng = 0;
						$lat = 0;
					endif;
					
					// Check, we don't want to add the item to the array if it does not have a $lat, $lng or $category_name					
					if(!$lng == 0 || !$lat == 0 || !$category_name == 0):
						
						if( $category_name == 'Developer'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/developer_map_icon.png';
						elseif ($category_name == 'Refurbishment'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/refurbishment_map_icon.png';
						elseif ($category_name == 'Contractor'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/contractor_map_icon.png';
						else:
							$marker_icon = 'http://maps.google.com/mapfiles/marker_green.png';
						endif;
						
			
						$project_map_data_array[] = array(
														  'latLng' => '['.$lat.', '.$lng.']', 
														  'data' => '\'<h2><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">'.$obj->post_title.'</a></h2><p>'.$obj->post_excerpt.'</p>\'', 
														  'options' => '{icon: \''.$marker_icon.'\'}'
														);										
					endif;
				endforeach;	
					
			endif;	
			return str_replace('"', '', str_replace('\\/', '/', json_encode($project_map_data_array)) );		
		endif;	
	}
	
	public function  getMap($post_id, $post_type, $post_tax) {
		
		global $wpdb;
		$project_map_data_array = array();
		
		// check that the post id is set
		if(isset($post_id) || isset($post_type)):
			
			// sql
			$sql = $wpdb->get_results("SELECT t1.ID, t1.post_title, t1.post_excerpt, t1.post_name, t1.guid, t3.obj FROM (SELECT * FROM `{$wpdb->prefix}posts`) AS t1
			INNER JOIN `{$wpdb->prefix}mappress_posts` AS t2 ON t1.id IN (t2.postid) INNER JOIN `{$wpdb->prefix}mappress_maps` AS t3 ON t2.mapid IN (t3.mapid) WHERE post_type='$post_type' AND t1.ID ='$post_id' ORDER BY t1.post_title ASC LIMIT 1");
			
		
			
			// check for results
			if(!empty($sql)):
				// loop over results				
				foreach($sql as $obj):

					$mapdata_unserialize = (array) unserialize($obj->obj);								
					
					$category = get_the_terms( $obj->ID, $post_tax );
					// only run if array is not empty
					if(!empty($category)):
						
						$category_name = $category[current(array_keys($category))]->name;
					else:
						$category_name = 0;
					endif;
					
					// only run if array is not empty
					if(!empty($mapdata_unserialize)):					
						$map_obj = $mapdata_unserialize['pois'][0]->point['lat'];						
						$lat = $mapdata_unserialize['pois'][0]->point['lat'];						
						$lng = $mapdata_unserialize['pois'][0]->point['lng'];						
					else:
						$lng = 0;
						$lat = 0;
					endif;
					
					// Check, we don't want to add the item to the array if it does not have a $lat, $lng or $category_name					
					if(!$lng == 0 || !$lat == 0 || !$category_name == 0):
						
						if( $category_name == 'Developer'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/developer_map_icon.png';
						elseif ($category_name == 'Refurbishment'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/refurbishment_map_icon.png';
						elseif ($category_name == 'Contractor'):
							$marker_icon = get_theme_root_uri().'/unitedhouse/images/contractor_map_icon.png';
						else:
							$marker_icon = 'http://maps.google.com/mapfiles/marker_green.png';
						endif;
						
			
						$project_map_data_array[] = array(
														  'latLng' => '['.$lat.', '.$lng.']', 
														  'data' => '\'<h2><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">'.$obj->post_title.'</a></h2><p>'.$obj->post_excerpt.'</p>\'', 
														  'options' => '{icon: \''.$marker_icon.'\'}'
														);										
					endif;
				endforeach;	
					
			endif;	
			return str_replace('"', '', str_replace('\\/', '/', json_encode($project_map_data_array)) );		
		endif;	
	}
	
}

?>