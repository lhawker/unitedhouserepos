<?
class BackgroundCarousel {
	
	function BackgroundCarouselSelection(){
		global $wpdb;
		if(is_front_page()):

			$sql = $wpdb->get_results("SELECT path, filename FROM `{$wpdb->prefix}ngg_gallery` INNER JOIN `{$wpdb->prefix}ngg_pictures` ON galleryid IN(gid) WHERE name = 'homepage-backgrounds'");
			
			if(!empty($sql)):
							
				echo '<script type="text/javascript">
				
				$( document ).ready(function() {
					/*
    					$.backstretch([';
						foreach($sql as $image):
							echo '"/'.$image->path.'/'.$image->filename.'",';
						endforeach;					
				 echo ' ], {        
							fade: 750,
        					duration: 3000
    					});
					*/	
						
						$.backstretch([';
						foreach($sql as $image):
							echo '"/'.$image->path.'/'.$image->filename.'",';
						endforeach;					
				 echo ' ]).cycle();
					});
										
    					
						</script>';
					echo '<div class="backstretch"></div>';	
			endif;			
		endif;
	}
}
?>