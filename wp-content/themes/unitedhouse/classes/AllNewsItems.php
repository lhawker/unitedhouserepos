<?php
	class AllNewsItems {
		
		private $featuredNewsItem;
		private $allOtherNewsItems;
		
		public function AllNews(){
			global $wpdb;
			$sql = $wpdb->get_results("SELECT ID, post_title, post_excerpt, post_name, post_date FROM `{$wpdb->prefix}posts` WHERE post_type = 'post' AND post_status = 'publish' ORDER BY post_date DESC");
			if(!empty($sql)):
				if($sql > 1):
					$this->featuredNewsItem = $sql[0];
					$this->allOtherNewsItems = $sql;
				endif;
			else:
				$this->featuredNewsItem = array();
				$this->$allOtherNewsItems = array();
			endif;
			// html output
			$this->FeaturedNewsItem();
			// html output
			$this->AllOtherNewsItems();
			
		}
		
		public function FeaturedNewsItem(){
			//echo '<article class="featuredNewsItem">';
			echo '<article style="border-top: none;" class="newsItems">';

			if(get_the_post_thumbnail($this->featuredNewsItem->ID) != ''):
				echo  '<span class="projects_cluster_large_img">'.get_the_post_thumbnail($this->featuredNewsItem->ID, 'latest-news-list-thumb').'</span>';
			endif;	
			$date = $this->post_date;
			$excerpt = $this->featuredNewsItem->post_excerpt;		
			$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
			$excerpt = strip_shortcodes($excerpt);
			$excerpt = strip_tags($excerpt);
			$excerpt = substr($excerpt, 0, 250);
			$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
			$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));		
		
		
			echo '	<header>
						<h3>
							<a href="/'.$this->featuredNewsItem->post_name.'" title="'.$this->featuredNewsItem->post_title.'">'.$this->featuredNewsItem->post_title.'</a>
						</h3>
					</header>
					<time>'.date_format(new DateTime($date), 'd/m/y').'</time>
					<p>'.$excerpt.'... <a href="/'.$this->featuredNewsItem->post_name.'" title="'.$this->featuredNewsItem->post_title.'"> Read more..</a></p>
				</article>';
				
				
				
		/** new template for this */
		
	
		
		
		
		}
		
		public function AllOtherNewsItems(){

			if(!empty($this->allOtherNewsItems)):
				//var_dump(array_splice($this->allOtherNewsItems, 1));
				
				
				if (class_exists('pagination')):
					$advanced_paginations = new pagination;
					$limit = get_option('posts_per_page');
					$items = $advanced_paginations->generate(array_splice($this->allOtherNewsItems, 1), $limit);
					
				
				echo '<div class="pagination pagination-left">'.$advanced_paginations->links().'</div>';
				
				foreach($items AS $news):
					
					echo '<article class="newsItems">';
												
					if(get_the_post_thumbnail($news->ID) != ''):
						echo  '<span class="projects_cluster_large_img">'.get_the_post_thumbnail($news->ID, 'latest-news-list-thumb').'</span>';
					endif;			
					$excerpt = $news->post_excerpt;		
					$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
					$excerpt = strip_shortcodes($excerpt);
					$excerpt = strip_tags($excerpt);
					$excerpt = substr($excerpt, 0, 250);
					$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
					$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));	
					echo '	<header>
							<h3>
								<a href="/'.$news->post_name.'" title="'.$news->post_title.'">'.$news->post_title.'</a>
							</h3>
							</header>
							<time>'.date_format(new DateTime($news->post_date), 'd/m/y').'</time>
							<p>'.$excerpt.'...<br/><a href="/'.$news->post_name.'" title="'.$news->post_title.'"> Read more...</a></p>';
						
						if (function_exists('fun_get_attachments')) {
							$check = fun_get_attachments(array('post_parent' => $news->ID));
							$post_mime_type = array('application/pdf','application/zip', 'text/plain', 'application/msword', 'application/vnd.ms-excel');
							
							/*if(!empty($check)):
								echo '<div id="downloads" class="project_attachments">';
								echo '<aside class="widget-download"><h3 class="widget-title">Downloads</h3>';
								foreach(fun_get_attachments(array('post_parent' => $news->ID)) as $attachment):
									
									$mineType = explode('/', $attachment->post_mime_type);
									
									if(isset($mineType)){
										if($mineType[0] != 'image'){
										echo '<a class="'.str_replace(".", "", $mineType[1]).'" href="'.site_url().$attachment->guid.'" target="_blank" title="'.$attachment->post_title.'"><span></span><strong>'.$attachment->post_title.'</strong></a>';
										}	
									}
																	
								endforeach;
							echo '</div>';
							endif;*/
						}
					echo '</article>';
						
				endforeach;
				echo '<hr class="pagination_clearfix"  />';
				echo '<div class="pagination pagination-left">'.$advanced_paginations->links().'</div>';
				endif; // pagination //
			endif;
			
		}
	}
?>