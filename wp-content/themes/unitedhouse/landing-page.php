<?php
/*
 * Template Name: Secondary Landing Page
 */

get_header(); ?>

<?php 
	
	
	$view_advert_image = get_post_meta(get_the_ID(), 'popup_advert-post-url', true);
	$view_advert_link = get_post_meta(get_the_ID(), 'popup_advert-page-url', true);
	
	if($view_advert_image !== '') {
	
	// Render the html
		echo '<!-- modal content -->
		<div id="basic-modal-content"><div class="popcontent">';
			
       echo '<img class="advert_image" src="'.$view_advert_image.'"><br/>';
		if($view_advert_link !== '') {
		echo '<p><a href="'.$view_advert_link.'" target="_blank" title="Read more"><span>Read more..<span></a>  <a class="simplemodal-close" title="Close"><span>Close<span></a></p>';
		} else {
		echo '<p><a class="simplemodal-close" title="Close"><span>Close<span></a></p>';
		}	
			
		echo'</div></div>';

		echo '<!-- preload the images -->
		<div style="display:none">
			<!-- <img src="img/basic/x.png" alt="" /> -->
		</div>
		<script type="text/javascript" src="'.get_template_directory_uri().'/js/jquery.simplemodal.js"></script>
		<script type="text/javascript">
			
	
		$(window).load(function() {
			$.modal($("#basic-modal-content"));
		});
		
		</script>
		<!-- Load jQuery, SimpleModal and Basic JS files -->
		';
	}
	
	?>

<script type="text/javascript">
	// A $( document ).ready() block.
$( document ).ready(function() {

	$('#projects_map').width('100%').height('350px').gmap3({
  		map:{
    		options:{
     			center:[51.607783, -0.109863],
     			zoom: 7,
				styles: [{featureType: 'all',stylers: [{ saturation: -90 },{ lightness: 50 }]}],
     			mapTypeControl: true,
				navigationControl: true,
     			scrollwheel: true,
     			streetViewControl: true
    		}
 		},

  		marker:{
    		values:<?php
				global $post;				
				$post_id = $post->ID;
				$post_type = $post->post_type;
				$post_type = 'projects_ct';
				$post_tax = 'projects';		
				$mapResults = new FindMaps();					
				// call function to return all log-lat from the maps.
				echo $mapResults->getCatMaps($post_id, $post_type, $post_tax, strtolower(get_the_title()));	
			?>,


    
			options:{ draggable: false },

    		events:{
      			click: function(marker, event, context){
        			var map = $(this).gmap3('get'),
          			infowindow = $(this).gmap3({get:{name:'infowindow'}});
        			if (infowindow){
          				infowindow.open(map, marker);
          				infowindow.setContent(context.data);
        			} else {
          				$(this).gmap3({
            				infowindow:{
              					anchor:marker, 
              					options:{content: context.data}
            				}
          				});
        			}
      			},
      			
    		}
  		}
	});
});
$(function () {
    $('#projectTabs a').click(function (e) {
  		e.preventDefault();
  		$(this).tab('show');
	});
	
	//projectTabs_refurbishertab_selector
	
	$('.projectTabs_refurbishertab_selector').click(function (e) {
  		$('.bd').removeClass('active');
	});
	$('.projectTabs_developertab_selector').click(function (e) {
  		$('.bd').removeClass('active');
	});
	$('.projectTabs_contractortab_selector').click(function (e) {
  		$('.bd').removeClass('active');
	});
	
	
});
</script>

	<div id="primary" class="content-area <?php echo strtolower(get_the_title()).'_selector';?>">

		<div id="content" class="site-content" role="main">
			<?php
				if(strtolower(get_the_title()) == 'contractor'){
					$style= '';
				}else{
					$style= '';
				}
			?>
			<ul <?php echo $style; ?> class="nav nav-pills <?php echo 'projectTabs_'.strtolower(get_the_title()).'_selector';?>" id="projectTabs">
  				<li class="active"><a href="#projectIntro">Introduction</a></li>
				<li><a href="#all_projects">All Projects</a></li>
				<?php
					if(strtolower(get_the_title()) == 'refurbisher'){
						echo '<div class="btn-group" style="margin-top: 2px;">
<a class="btn btn-danger dropdown-toggle projectTabs_'.strtolower(get_the_title()).'tab_selector" data-toggle="dropdown" href="#">Filter Projects by<span class="caret"></span></a>';
						echo '<ul class="dropdown-menu">';
						echo '<li class="bd"><a href="#refurbishments">Refurbishment</a></li>';
						echo '<li class="bd"><a href="#lowcarbon">Low-carbon Retrofit</a></li>';
						echo '<li class="bd"><a href="#active_WHISCERS">WHISCERS</a></li>';
						echo '</ul></div>';
					} elseif(strtolower(get_the_title()) == 'contractor') {
						
						echo '<div class="btn-group" style="margin-top: 2px;">
<a class="btn btn-danger dropdown-toggle projectTabs_'.strtolower(get_the_title()).'tab_selector" data-toggle="dropdown" href="#">Filter Projects by<span class="caret"></span></a>';

						echo '<ul class="dropdown-menu">';
						
						echo '<li class="bd"><a href="#shnb">Social Housing New Build</a></li>';
						echo '<li class="bd"><a href="#privateres">Private Residential</a></li>';
						echo '<li class="bd"><a href="#mixeduse">Mixed-use</a></li>';
						echo '<li class="bd"><a href="#ppp">PPP</a></li>';
						echo '<li class="bd"><a href="#regeneration">Regeneration</a></li>';
						echo '<li class="bd"><a href="#carehomes">Care Homes</a></li>';
						echo '<li class="bd"><a href="#keyworkers">Key Worker</a></li>';
						echo '<li class="bd"><a href="#highrise">High-rise</a></li>';
						
						echo '</ul></div>';
					} elseif(strtolower(get_the_title()) == 'developer') {
						echo '<div class="btn-group" style="margin-top: 2px;">
<a class="btn btn-danger dropdown-toggle projectTabs_'.strtolower(get_the_title()).'tab_selector" data-toggle="dropdown" href="#">Filter Projects by<span class="caret"></span></a>';

						echo '<ul class="dropdown-menu">';
						echo '<li class="bd"><a href="#active_projects">Active Projects</a></li>
  						<li class="bd"><a href="#completed_projects">Completed Projects</a></li>';

						echo '</ul></div>';
					}
				?>
  				
				
			
  			</ul>
 
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
				
				<div class="tab-content">
  					<div class="tab-pane active" id="projectIntro">
						<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<!--<header class="entry-header">
								<h1 class="entry-title"><?php //the_title(); ?></h1>
							</header> --><!-- .entry-header -->				
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
						</article><!-- #post -->
					</div>
					<div class="tab-pane" id="all_projects">
						<?php
							if(strtolower(get_the_title()) == 'developer'):
								$current_taxonomy_id = 20;
							elseif(strtolower(get_the_title()) == 'contractor'):
								$current_taxonomy_id = 21;
							elseif(strtolower(get_the_title()) == 'refurbisher'):
								$current_taxonomy_id = 2;
							else:
								$current_taxonomy_id = 0;
							endif;	
								
							$listAllProjectsByQuery = new ListAllProjectsTaxonomys();
							$returnedProjects_array = $listAllProjectsByQuery->listProjectsByQuery($current_taxonomy_id, 5, 'DEFAULT_PROJECTS');
							
						
				echo '<section class="project_tagged_clusters">';
				if(!empty($returnedProjects_array)):
					echo '<div class="carousel slide" id="Carousel_all">
					<div class="carousel-inner">';
				$i = 0;
				foreach ($returnedProjects_array as $project):
					$i = $i + 1;
					if($i > 1):
						$active_css_class = '';
					else:
						$active_css_class = 'active';
					endif;
						
					echo '<!-- loop here -->
              			<div class="item span12 '.$active_css_class.'">
                    		<ul class="thumbnails">
                        		<li class="span5">
                            		<div>';
									if(get_the_post_thumbnail($project->ID) != ''):
										$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($project->ID, 'project-carousel-image'));
										echo  '<span class="projects_cluster_large_img">'.$img_string.'</span>';
										endif;
					$excerpt = $project->post_excerpt;		
					$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
					$excerpt = strip_shortcodes($excerpt);
					$excerpt = strip_tags($excerpt);
					$excerpt = substr($excerpt, 0, 220);
					$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
					$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));	
					
					
                     echo '</div>
                        		</li>
								<li class="span7">
                            		<h2>'.$project->post_title.'</h2>
									<p>'.$excerpt.'</p>
									<p><a href="/projects_ct/'.$project->post_name.'" title="">Read more...</a></p>
                        		</li>
                    		</ul>
              			</div>
						<!-- end loop here -->';
				endforeach;

			echo '</div>';
			
			if(count($returnedProjects_array) > 1):
            	echo'<a data-slide="prev" href="#Carousel_all" class="left carousel-control">‹</a>
            		<a data-slide="next" href="#Carousel_all" class="right carousel-control">›</a>';
			endif;
			
        	echo '</div>';
				else:
					echo '<h2>Sorry, but there are no "'.$project_tag_status.'" projects at this time!</h2>';
				endif;
			echo '</section>';
							
							
						?>
					</div><!-- #active_projects -->
  					<div class="tab-pane" id="active_projects">
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'active';					
							
								if(strtolower(get_the_title()) == 'developer'):
									$current_taxonomy_id = 20;
								elseif(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								elseif(strtolower(get_the_title()) == 'refurbisher'):
									$current_taxonomy_id = 2;
								else:
									$current_taxonomy_id = 0;
								endif;							

							$getActiveOrPassProjectsTaxonomyActiveTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyActiveTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div><!-- #active_projects -->
  					<div class="tab-pane" id="completed_projects">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'completed';
								
								if(strtolower(get_the_title()) == 'developer'):
									$current_taxonomy_id = 20;
								elseif(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								elseif(strtolower(get_the_title()) == 'refurbisher'):
									$current_taxonomy_id = 2;
								else:
									$current_taxonomy_id = 0;
								endif;
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					
					<div class="tab-pane" id="shnb">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Social housing new build';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					<div class="tab-pane" id="highrise">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'High-rise';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					<div class="tab-pane" id="keyworkers">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Key worker';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					<div class="tab-pane" id="privateres">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Private residential';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					<div class="tab-pane" id="mixeduse">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Mixed-use';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					<div class="tab-pane" id="ppp">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'PPP';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					
					<div class="tab-pane" id="regeneration">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Regeneration';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					
					<div class="tab-pane" id="carehomes">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Care homes';
								
								if(strtolower(get_the_title()) == 'contractor'):
									$current_taxonomy_id = 21;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					
					<div class="tab-pane" id="refurbishments">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Refurbishments';
								
								if(strtolower(get_the_title()) == 'refurbisher'):
									$current_taxonomy_id = 2;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					
					<div class="tab-pane" id="lowcarbon">
	
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'Low-carbon retrofit';
								
								if(strtolower(get_the_title()) == 'refurbisher'):
									$current_taxonomy_id = 2;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div>
					
											
						
					<!-- WHISCERS tab pane -->
					<div class="tab-pane" id="active_WHISCERS">
						<?php
							$current_taxonomy = 'projects';
							$tag_tax_type = 'projects_tags';
							$project_tag_status = 'whiscers';
								
								if(strtolower(get_the_title()) == 'refurbisher'):
									$current_taxonomy_id = 2;
								else:
									$current_taxonomy_id = 0;
								endif;	
							
							$getActiveOrPassProjectsTaxonomyCompletedTags = new ListAllProjectsTaxonomys();					
							// call function to return all log-lat from the maps.				
							echo $getActiveOrPassProjectsTaxonomyCompletedTags->getActiveOrPassProjectsTaxonomyTags($current_taxonomy, $tag_tax_type, $project_tag_status, $current_taxonomy_id);

							?>
					</div><!-- #active_projects -->
					
					<?php
					if (function_exists('userTypeAttachments')) {
							userTypeAttachments();
					}
					?>
  				</div>
				<br/>
				<div class="row-fluid">
					<div class="span4 print_random_news">
						<div class="print_wrap">
						<h2 class="sub_heading">News</h2>
						<?php
							$randomNewsItem = new RandomNews();
							echo $randomNewsItem->getRandomNews();	
						?>
						</div>
					</div>
  					<div class="span8 print_video">
						<h2 class="sub_heading">Video</h2>
						<?php echo do_shortcode( '[featured-video-plus]' ) ?>
					</div>
				</div>
				<br/>
				<?php //comments_template(); ?>
			<?php endwhile; ?>
			
			<!-- map and ajax -->
			<div class="map_area">
				<h2 class="sub_heading">Project Locations</h2>
				<div id="projects_map">map pleases</div>
				<!-- <div class="map_key">
					<ul>
						<li>Map Key:</li>
						<li class="developments_map_icon"><span>&nbsp;</span>Developments</li>
						<li class="refurbishment_map_icon"><span>&nbsp;</span>Refurbishment</li>
						<li class="contractor_map_icon"><span>&nbsp;</span>Contractor</li>
					</ul>
				</div> -->
			</div>			
			<br/>
			<?php
				
				$current_taxonomy = 'projects';			
				
				if(strtolower(get_the_title()) == 'developer'):
					$current_taxonomy_id = 20;
				elseif(strtolower(get_the_title()) == 'contractor'):
					$current_taxonomy_id = 21;
				elseif(strtolower(get_the_title()) == 'refurbisher'):
					$current_taxonomy_id = 2;
				else:
				$current_taxonomy_id = 0;
				endif;					
				
				$ajax_link_selector = '#tax_filter';
				$ajax_display_selector = '#ajax-status';
				$listAllProjectsTaxonomys = new ListAllProjectsTaxonomys();					
				// call function to return all log-lat from the maps.
				echo '<h2 class="sub_heading" style="float: left;">Projects</h2>';
				echo '<nav class="project_filter_nav">';
				echo $listAllProjectsTaxonomys->getAllProjectsTaxonomys($current_taxonomy, $current_taxonomy_id, $ajax_link_selector, $ajax_display_selector);
				echo '</nav>';	
			?>

			<div id="ajax-status">
			<!-- loop in content -->
												
	<?php 
		$listAllProjectsByQuery = new ListAllProjectsTaxonomys();
		$returnedProjects_array = $listAllProjectsByQuery->listProjectsByQuery($current_taxonomy_id, 5, 'DEFAULT_PROJECTS');
		if(!empty($returnedProjects_array)):
					$i = 0;
					echo '<section class="projects_cluster">';
					foreach($returnedProjects_array as $obj):
						if($i > 4):
							$i = 1;
						else:
							$i = $i + 1;
						endif;
						$excerpt = $obj->post_excerpt;
						$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
						$excerpt = strip_tags($excerpt);
						$excerpt = substr($excerpt, 0, 50);
						$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
						$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));	
						
						$title = $obj->post_title;
						$title = preg_replace(" (\[.*?\])",'',$title);
						$title = strip_tags($title);
						$title = substr($title, 0, 50);
						$title = substr($title, 0, strripos($title, " "));
						$title = trim(preg_replace( '/\s+/', ' ', $title));	
						
						
						if($i == 1):				
							echo '<article class="projects_cluster_large">';							
							if(get_the_post_thumbnail($obj->ID) != ''):
								$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($obj->ID, 'large'));
								echo  '<span class="projects_cluster_large_img">'.$img_string.'</span>';
							endif;
							echo '<div class="projects_cluster_large_content">';
							echo  '<header><h2>'.$obj->post_title.'</h2></header>';
							echo  '<p>'.$excerpt.' ...</p>';
							echo  '<p><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">Read more ...</a></p>';
							echo '</div>';
							echo '</article>';
						elseif($i > 1):
							echo '<article class="projects_cluster_small">';
							if(get_the_post_thumbnail($obj->ID) != ''):
								$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($obj->ID, 'large'));
								echo  '<span class="projects_cluster_small_img">'.$img_string.'</span>';
							endif;
							echo '<div class="projects_cluster_small_content">';							
							echo  '<header><h2><strong style="font-weight: normal;">'.$title.'</strong></h2></header>';
							echo  '<p>'.$excerpt.' ...</p>';
							echo  '<p><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">Read more ...</a></p>';
							echo '</div>';
							echo '</article>';
						endif;
						if($i > 5):
							echo '<div class="clearfix">&nbsp;</div>';
						endif;
					endforeach;
					echo '<div class="clearfix">&nbsp;</div>';
					echo '</section>';				
				endif;
	?>
		<div id='loadingDiv'>
    		Loading please wait...
 		</div> 
			</div>
			<!-- map and ajax -->

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>