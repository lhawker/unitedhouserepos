<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 */
?>

<?php if ( is_search() ) : // Only display Excerpts for Search ?>
	<div>
		<h3 class="entry-title">
			<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
		</h3>
		<?php the_excerpt(); ?>
	</div><!-- .entry-summary -->
	
	<?php else: ?>
		
	<div class="row-fluid">
		<div class="span12">
		<?php if ( has_post_thumbnail() && ! post_password_required() ) : ?>
			<div class="entry-thumbnail">
				<?php 
					echo preg_replace("/(width|height)=\"[0-9]*\"/i", "", get_the_post_thumbnail()); 
					// the_post_thumbnail();
					?>
				</div>
		<?php endif; ?>
    	</div>
	</div>
	<div class="row-fluid">
		<div class="span9">
      	<!--Sidebar content-->
			<div class="content_wrapper">
				<section class="content" rel="content">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<header class="entry-header">
						<?php if ( is_single() ) : ?>
							<h1 class="entry-title"><?php the_title(); ?></h1>
						<?php else : ?>
						<h2>
							<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
						</h2>
						<?php endif; // is_single() ?>
						</header><!-- .entry-header -->
						<?php the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentythirteen' ) ); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</article>
				</section>
			</div>
    	</div>
    	<div class="span3 widget_column">
      	<!--Body content-->
			<div class="content_wrapper">
				<?php
					if (function_exists('userTypeAttachments')) {
						userTypeAttachments();
					}
					get_sidebar('main');
					?>
			</div>
    	</div>
	</div>
	<?php 
		if (function_exists('retunMap')){
			retunMap(get_the_ID());
		}		 
	?>
	
	<? if (comments_open()){ ?>
	<div class="row-fluid">
		<div class="span9">
			<div class="content_wrapper">
				<section class="comments" rel="comments">
					<?php comments_template();  ?>
				</section>
			</div>
		</div>
	</div>
	<? } ?>
	
<?php endif;?>

