<?php

/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage United_House
 * @since United House 1.0
 */
?>

<!DOCTYPE html>
<!--[if IE 7]>
	<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
	<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php bloginfo( 'name' ); echo ' '; wp_title(); ?></title>
	
	<meta http-equiv="imagetoolbar" content="false">
	<meta name="author" content="United House" />
	<meta name="copyright" content="United House" />
	<meta name="meta_keywords" content="<?php page_meta_keywords(); ?>">	
	<meta name="meta_description" content="<?php page_meta_description(get_the_ID()); ?>">
	
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />


	<!--[if lt IE 9]>
		<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js" type="text/javascript" charset="utf-8"></script>
		<script src="<?php echo get_template_directory_uri(); ?>/js/css3-mediaqueries.js" type="text/javascript" charset="utf-8"></script>
	<![endif]-->
	
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
	<?php wp_head(); ?>
	
	<!--[if IE 6]>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/stylesheets/css/ie.css"" />
		<script src="<?php echo get_template_directory_uri(); ?>/js/old_ies_popup.js" type="text/javascript" charset="utf-8"></script>
	<![endif]-->
	<!--[if IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/stylesheets/css/ie.css"" />
		<script src="<?php echo get_template_directory_uri(); ?>/js/old_ies_popup.js" type="text/javascript" charset="utf-8"></script>
	<![endif]-->
	
	<!--[if IE 8]>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/stylesheets/css/ie8.css"" />
	<![endif]-->
	
	<!--[if IE 9]>
		<link rel="stylesheet" type="text/css" media="all" href="<?php echo get_template_directory_uri(); ?>/stylesheets/css/ie9.css"" />
	<![endif]-->
	
	<?php
		if(is_front_page()):
			echo '<script type="text/javascript">			
				$(document).ready(function(){
								$(".social_button_wrapper").appendTo("#masthead");
				});		  
			</script>';
		elseif(get_post_meta($post->ID,'_wp_page_template',true) == 'default' || get_post_meta($post->ID,'_wp_page_template',true) == 'projects-landing-page.php' || get_post_meta($post->ID,'_wp_page_template',true) == 'news-landing-page.php' || is_single() ):
			echo '<script type="text/javascript">			
				$(document).ready(function(){
								$(".social_button_wrapper").appendTo(".span12");
				});		  
			</script>';
			
		elseif(get_post_meta($post->ID,'_wp_page_template',true) == 'contact-page.php'):
			echo '<script type="text/javascript">			
				$(document).ready(function(){
						$(".social_button_wrapper").appendTo(".sm");
						$(".attachment_description").appendTo(".contact-download-pod");
						
				});		  
			</script>';
			
			
		else:
			echo '<script type="text/javascript">			
				$(document).ready(function(){
								$(".social_button_wrapper").prependTo("#content");
				});		  
			</script>';
		endif;
		
		echo '<script type="text/javascript">			
				$(document).ready(function(){
					$(".attachment_description").prependTo(".project_attachments");
				});		  
			</script>';
	?>
	
	
</head>

	
	<?php 
	$current_tax_name = get_the_terms($post->id, 'projects');

	if(!empty($current_tax_name)):
		$current_section_css_id = strtolower($current_tax_name[current(array_keys($current_tax_name))]->name).'_section';
	else:
		$current_section_css_id = '';
	endif;

	?>
	
	
<body <?php body_class(); ?> id="<?php echo $current_section_css_id; ?> top">
	<div class="row-fluid" rel="cookie_agree_box"></div>

	<div class="header-area <?php echo strtolower(get_the_title()).'_selector';?>">
		<div id="page" style="background: transparent;" class="container">
			<!-- ******* header start ***** -->
	<header id="masthead" class="site-header" role="banner">
		<div class="site_logo" rel="site_logo">
			<h1>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="reset">
					<img width="auto" height="auto" src="<?php echo get_template_directory_uri(); ?>/images/site_logo.gif" title="<?php bloginfo( 'name' ); ?>" alt="<?php bloginfo( 'name' ); ?>" />
				</a>
			</h1>
		</div>
		<div class="site_search">
			<a href="/request-newsletter" class="btn btn-info" title="News feed sign up">Newsletter sign up</a>		
			<div class="input-append">
				<form role="search" method="get" id="searchform" class="searchform" action="/">
					<label class="screen-reader-text" for="s" style="display: none;">Search for:</label>
  					<input value="Search United House" name="s" class="span2" id="appendedInputButton" type="text" onfocus="if (this.value == 'Search United House') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search United House';}"/>
  					<button id="searchsubmit" class="btn" type="submit"><span>Go!</span></button>
				</form>
			</div>			
		</div>
		
		<nav class="section_select_buttons">
				<ul class="<?php echo get_the_title();?>">
					<li class="developer_button"><a href="/developer" title="Developer">Developer</a></li>
					<li class="contractor_button"><a href="/contractor" title="Contractor">Contractor</a></li>
					<li class="refurbisher_button"><a href="/refurbisher" title="Refurbisher">Refurbisher</a></li>
				</ul>
		</nav>
		
		<h2 class="site-description hidden"><?php bloginfo( 'description' ); ?></h2>
		
		<!-- *********************** New navigation ************************** START -->
		<div class="navbar">
			<div class="navbar-inner" role="">
				<div class="container" role="">
				<!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      				<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        				<span class="icon-bar"></span>
        				<span class="icon-bar"></span>
        				<span class="icon-bar"></span>
      				</a>
					<a class="brand hidden-desktop" name="menu"><?php _e( 'Menu', 'twentythirteen' ); ?></a>
						
					<nav class="nav-collapse collapse">
						<div class="nav-menu">
							<ul class="nav">
								<?php
									wp_nav_menu( array(
    									'theme_location'    => 'primary',
    									'container'     =>  false,
    									'container_id'      => false,
    									'conatiner_class'   => false,
    									'menu_class'        => false, 
    									'echo'          => true,
    									'items_wrap'        => '%3$s',
    									'depth'         => 0, 
    									'walker'        => new themeslug_walker_nav_menu
									) ); // thanks nick
								?>	
							</ul>
						</div>
					</nav>
					<a class="screen-reader-text skip-link" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentythirteen' ); ?>"><?php _e( 'Skip to content', 'twentythirteen' ); ?></a>
				</div>
			</div>
		</div>
		<!-- *********************** New navigation ************************** END -->
		<?php
			if(!is_front_page()):
				$foo = new bootstrap_breadcrumbs;
				$foo->get_breadcrumbs('breadcrumb');
			endif;			
		?>
		</header><!-- #masthead -->
		</div>
	</div><!-- .header-area -->

	<div id="page" class="container">
	<div class="large_banners">
		<div id="branding">Loading</div>
		<div class="large_banner_txt <?php echo strtolower(get_the_title()).'_selector_text';?>">
		<?php the_excerpt(); ?> 	
		</div>
	</div>
	</div>
	<div class="testimage"></div>
	<div id="page" style="background: transparent;" class="container">
		<div id="main" class="site-main">