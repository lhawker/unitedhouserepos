<?php
// Include the nav walker class
//require 'classes/class_nav_walker.php';
// Include the nav walker class
//require 'classes/class_breadcrumbs.php';

// Include this class into the functions file
/*require get_template_directory() . '/classes/FindMaps.php';
require get_template_directory() . '/classes/list_all_projects_taxonomys.php';
require get_template_directory() . '/classes/BackgroundCarousel.php';
require get_template_directory() . '/classes/RandomNews.php';
require get_template_directory() . '/classes/AllProjects.php';*/

// include all classes from
$classes_directory_path = get_template_directory() . '/classes';



require get_template_directory() . '/advert_metabox.php';

function include_all_php($folder){
    foreach (glob("{$folder}/*.php") as $filename)
    {
        require $filename;
    }
$randomNewsItem = new RandomBanner();
							echo $randomNewsItem->getRandomBanner();
}

include_all_php("$classes_directory_path");


/******************************************************************************/
/* get meta page description*/

function page_meta_description($id) {
     echo strip_tags(the_excerpt_rss($id));

}

function page_meta_keywords() {
     echo 'Homes, housing, affordable housing, developer, regeneration, sustainability, new build, private housing, housing provider, United House, social housing, refurbishment, retrofit, low carbon, housebuilder, private residential, high end';

}
/******************************************************************************/


/******************************************************************************/
/* ADD EXCERTPTS TO PAGE TYPE FUNCTION */
add_action( 'init', 'add_excerpts_to_pages' );
function add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/******************************************************************************/

/******************************************************************************/
/* PAGINATION MENU FUNCTION */
if ( ! function_exists( 'pagination' ) ) : 
function pagination () { 
global $wp_query, $wp_rewrite; 
	$pages =''; 
	$max = $wp_query ->max_num_pages; 
	
	if (! $current = get_query_var ('paged')) $current = 1; 
		$a ['base'] = str_replace (999999999, '%#%', get_pagenum_link (999999999)); 
		$a ['total'] = $max; 
		$a ['current'] = $current; 

		$total = 1;// 1 - show the text "Page N of N", 0 - do not display 
		$a ['mid_size'] = 3; // show how many links on the left and right of the current 
		$a ['end_size'] = 1; // how many references show the beginning and end of the 
		$a ['prev_text'] = '«'; // link text" Previous Page " 
		$a ['next_text'] = '»'; // link text" Next Page " 

		if ($max> 1) echo ''; 
		if ($total == 1 && $max> 1) $pages = ' Page'. $current. ' of '. $max.'  '. " - "; 
		//echo $pages;
		echo '<div class="pagination pagination-left"><ul>';
		echo '<li>'.paginate_links($a).'</li>'; 
		echo '</ul></div>';
		if ($max> 1) echo ''; 
	}
endif;

/******************************************************************************/
/* FIND ALL DOCUMENT ATTACHMENTS ATTACHED TO THE PAGE THEN CHECK THE USER
   COOKIE AND RETURN THE CORRECT ATTACHMENT, TO THAT USER */

function userTypeAttachments(){
	global $wpdb;
	$checkArray = fun_get_attachments( );
	
	$checkArrayForImages = array();
	
	foreach(fun_get_attachments( ) as $item){
		$mineType = explode('/', $item->post_mime_type);
		if(isset($mineType)){
			if($mineType[0] != 'image'){
				if(!in_array($item->ID, array($checkArrayForImages))){
					//$attachmeIdArray .= $item->ID;
					array_push($checkArrayForImages, $item->ID);
				}			
			}
		}	
	}
	

	if(!empty($checkArrayForImages)){
		echo '<div class="project_attachments '.strtolower(get_the_title()).'_download_selector">';
		echo '<aside class="widget-download"><h3 class="widget-title">Downloads</h3>';
	}
	
	$attachmeIdArray = array();
	foreach(fun_get_attachments( ) as $item){
		$mineType = explode('/', $item->post_mime_type);	
		if(isset($mineType)){
			if($mineType[0] != 'image'){
				if(!in_array($item->ID, array($attachmeIdArray))){
					//$attachmeIdArray .= $item->ID;
					array_push($attachmeIdArray, $item->ID);
				} 
			
			}
		}
	}

	$friendsArray2 = '"' . implode('","', $attachmeIdArray) . '"';
	

	$sqlMapQueary =$wpdb->get_results( "SELECT a.ID, a.post_title, a.post_content, a.guid, a.post_mime_type, b.meta_id, b.post_id, b.meta_value FROM {$wpdb->prefix}posts a INNER JOIN {$wpdb->prefix}postmeta b ON a.id=b.post_id WHERE b.post_id IN ($friendsArray2) AND b.meta_key = '_attachmentGroup'" );


	foreach($sqlMapQueary as $obj) {
		$itemArray = unserialize($obj->meta_value);
		$userSelection = 'contractor'; // now get this value from the cookie

		if (isset($_COOKIE["userselect"])){
			$userSelection = $_COOKIE["userselect"]; // now get this value from the cookie
		} else {
			$userSelection = 'justShowAll';
		}
 		


		if(in_array($userSelection, $itemArray)){
			$mineType = explode('/', $obj->post_mime_type);
			/*$htmlString = '<a class="'.str_replace('.','', $mineType[1]).'" href="'.$obj->guid.'" target="_blank" title="Download '.$obj->post_title.'"><span></span><strong>'.$obj->post_title.'</strong></a><div class="attachment_description">'.preg_replace('/\s+?(\S+)?$/', '', substr($obj->post_content, 0, 150)).'</div>';*/
			$htmlString = '<a class="'.str_replace('.','', $mineType[1]).'" href="'.$obj->guid.'" target="_blank" title="Download '.$obj->post_title.'"><span></span><strong>'.$obj->post_title.'</strong></a><div class="attachment_description">'.$obj->post_content.'</div>';
			echo $htmlString;
		} else {
			// just print all atached files
			if($userSelection == 'justShowAll'){
				$mineType = explode('/', $obj->post_mime_type);
				/*$htmlString = '<a class="'.str_replace('.','', $mineType[1]).'" href="'.$obj->guid.'" target="_blank" title="Download '.$obj->post_title.'"><span></span><strong>'.$obj->post_title.'</strong></a><div class="attachment_description">'.preg_replace('/\s+?(\S+)?$/', '', substr($obj->post_content, 0, 150)).'</div>';*/
				$htmlString = '<a class="'.str_replace('.','', $mineType[1]).'" href="'.$obj->guid.'" target="_blank" title="Download '.$obj->post_title.'"><span></span><strong>'.$obj->post_title.'</strong></a><div class="attachment_description">'.$obj->post_content.'</div>';
				echo $htmlString;
			}
		}
		
		
	}
	if(!empty($checkArrayForImages)){
		echo '</aside>';
		echo '</div>';
	}
}

/******************************************************************************/
/* FIND THE PAGES MAP ID */

function retunMap($postID){
	if(isset($postID)){
		global $wpdb;
		$sqlMapQueary =$wpdb->get_results( "SELECT mapid FROM {$wpdb->prefix}mappress_posts WHERE postid = $postID LIMIT 1" );
		foreach($sqlMapQueary as $item){
			$shortcode_content = '[mappress width="100%" mapid='.$item->mapid.' adaptive="true"]';
			echo '<div class="row-fluid"><div class="span9"><div class="content_wrapper"><secction id="map">'.do_shortcode($shortcode_content).'</secction></div></div></div>';			
		}
	}
}
/******************************************************************************/
/* REMOVE THE WORDPRESS VERSION FROM THE HEADER */
function wp_remove_version() {
	return '';
}
	
add_filter('the_generator', 'wp_remove_version');
/******************************************************************************/
add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 604, 270, true );

/************************* CUSTOM COMMENT RENDERER ****************************/
function mytheme_comment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
   <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
     
	<div id="comment-<?php comment_ID(); ?>">
      <div class="pull-left">
         <?php echo get_avatar($comment,$size='34',$default='http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536' ); ?>
      </div>
      <?php if ($comment->comment_approved == '0') : ?>
         <em><?php _e('Your comment is awaiting moderation.') ?></em>
         <br />
      <?php endif; ?>

      <div class="media-body"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?>
		<br/>
         <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
         	
         <?php comment_text() ?>
		<div class="reply">
         	<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
      	</div>
	 </div>

      
     </div>
<?php
        }



/******************************************************************************/
/* REGISTER WIDGET AREAS */
function default_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Main Widget Area', 'unitedhouse' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears on all page or news / blog sections.', 'unitedhouse' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

	register_sidebar( array(
		'name'          => __( 'Secondary Widget Area', 'unitedhouse' ),
		'id'            => 'main',
		'description'   => __( 'Appears on all case study pages.', 'unitedhouse' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'default_widgets_init' );

/******************************************************************************/
/* ADD THEME CUSTOMIZE SUPPORT */
function theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'logoOne' )->transport = 'postMessage';

	$wp_customize->add_section('themename_footer_logos', array(
        'title'    => __('Footer Logos', 'themename'),
        'priority' => 120,
    ));

	// Footer logo image selection one options start
	$wp_customize->add_setting( 'logoOne[image_url]', array(
    	'default' => 'http://www.unitedhouse.net',
    	'type' => 'option',
		'transport'   => 'refresh',
		) );
			
	$wp_customize->add_control( 'logoOne[image_url]', array(
    	'label' => 'Logo image one URL:',
    	'section' => 'themename_footer_logos',
		'settings'   => 'logoOne[image_url]',
		) );
	
	$wp_customize->add_setting( 'logoOne[image_upload]', array(
    	'default' => '/images/blank_logo.gif',
    	'type' => 'option',
		'transport'   => 'refresh',
		) );
	
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logoOne[image_upload]', array(
    'label'    => __('Logo image one Upload', 'themename'),
    'section'  => 'themename_footer_logos',
    'settings' => 'logoOne[image_upload]',
	)));
	// Footer logo image selection one options end
	// *********************************** >
	// Footer logo image selection two options start
	$wp_customize->add_setting( 'logoTwo[image_url]', array(
    	'default' => 'http://www.unitedhouse.net',
    	'type' => 'option',
		'transport'   => 'refresh',
			) );
			
	$wp_customize->add_control( 'logoTwo[image_url]', array(
    	'label' => 'Logo image two URL:',
    	'section' => 'themename_footer_logos',
		'settings'   => 'logoTwo[image_url]',
	) );
	
	$wp_customize->add_setting( 'logoTwo[image_upload]', array(
    	'default' => '/images/blank_logo.gif',
    	'type' => 'option',
		'transport'   => 'refresh',
			) );
	
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logoTwo[image_upload]', array(
    'label'    => __('Upload, logo must be (20px W X 20px H)', 'themename'),
	'helper' => __('Logo image two Upload', 'themename'),
    'section'  => 'themename_footer_logos',
    'settings' => 'logoTwo[image_upload]',
	)));
	// Footer logo image selection two options end
	
	/*ffffffff*/
	// *********************************** >
	// Footer logo image selection two options start
	$wp_customize->add_setting( 'logoThree[image_url]', array(
    	'default' => 'http://www.unitedhouse.net',
    	'type' => 'option',
		'transport'   => 'refresh',
			) );
			
	$wp_customize->add_control( 'logoThree[image_url]', array(
    	'label' => 'Logo image two URL:',
    	'section' => 'themename_footer_logos',
		'settings'   => 'logoThree[image_url]',
	) );
	
	$wp_customize->add_setting( 'logoThree[image_upload]', array(
    	'default' => '/images/blank_logo.gif',
    	'type' => 'option',
		'transport'   => 'refresh',
			) );
	
	$wp_customize->add_control( new WP_Customize_Image_Control($wp_customize, 'logoThree[image_upload]', array(
    'label'    => __('Upload, logo must be (20px W X 20px H)', 'themename'),
	'helper' => __('Logo image two Upload', 'themename'),
    'section'  => 'themename_footer_logos',
    'settings' => 'logoThree[image_upload]',
	)));
	// Footer logo image selection two options end
	//http://abandon.ie/exploring-wordpress-theme-customizer/
	//http://wp.tutsplus.com/tutorials/theme-development/digging-into-the-theme-customizer-practicing-ii/
}
add_action( 'customize_register', 'theme_customize_register' );

/**
 * Binds JavaScript handlers to make Customizer preview reload changes
 * asynchronously.
 *
 * @since Twenty Thirteen 1.0
 */
//wp_enqueue_script('my_script', get_template_directory_uri() . '/js/cookie_agree.js', array('jquery'), true);
function theme_customize_preview_js() {
	wp_enqueue_script( 'theme-customizer', get_template_directory_uri() . '/js/theme-customizer.js', array( 'customize-preview' ), '20130226', true );
//	wp_enqueue_script( 'theme-customizer', get_template_directory_uri() . '/js/cookie_agree.js', array( 'customize-preview' ), '20130226', true );
}
function cookie_agree_script_init() {
	wp_enqueue_script('my_script', get_template_directory_uri() . '/js/cookie_agree.js', array('jquery'));
}

function gmap3_script_init() {	
	wp_enqueue_script('googlemapapi', 'http://maps.google.com/maps/api/js?sensor=false&amp;language=en', array('jquery'));
	wp_enqueue_script('gmap3', get_template_directory_uri() . '/js/gmap3.min.js', array('jquery'));
}

function backstretch_scripts_init(){
	wp_enqueue_script('backstretch', get_template_directory_uri() . '/js/jquery.backstretch.js');
}

add_action( 'wp_enqueue_scripts', 'gmap3_script_init', 10);
//add_action( 'wp_enqueue_scripts', 'backstretch_scripts_init', 11);
add_action( 'wp_enqueue_scripts', 'backstretch_scripts_init', 12);

add_action( 'customize_preview_init', 'theme_customize_preview_js' );
add_action( 'wp_enqueue_scripts', 'cookie_agree_script_init', 10);
/******************************************************************************/
/* New thubnail image sizes */
if ( function_exists( 'add_image_size' ) ) { 
	add_image_size( 'latest-news-list-thumb', 200, 130 , true); //300 pixels wide (and unlimited height)
	add_image_size( 'latest-news-home-thumb', 225, 100, true ); //(cropped)
	add_image_size( 'default-page-large-image', 850, 380, true ); //(cropped)
	add_image_size( 'project-carousel-image', 470, 330, true ); //(cropped)
}
function change_mce_options($init){
    $init["forced_root_block"] = false;
    $init["force_br_newlines"] = true;
    $init["force_p_newlines"] = false;
    $init["convert_newlines_to_brs"] = true;
    return $init;       
}
add_filter('tiny_mce_before_init','change_mce_options');
function custom_colors() {
   echo '<style type="text/css">
           body hr { 
    		font-family: Arial, Helvetica, sans-serif; 
    		margin: 10px; 
			display: none!important;
			}

			body#tinymce.wp-editor a {
    			color: #4CA6CF;
			}
         </style>';
}

add_action('admin_head', 'custom_colors');
?>