<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<div class="row-fluid">
			<div class="span12">
				<?php
					// load featured image
					if ( has_post_thumbnail() ) {
						/*$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail());*/
						
					
						$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($post->ID, 'default-page-large-image'));
						
					
						echo  '<div class="featured_image"><span class="projects_cluster_large_img">'.$img_string.'</span></div>';
					} 
				?>
			</div><!-- .span12 -->
		</div><!-- .row-fluid -->
		<div class="row-fluid">
			<div class="span12">
				<header class="entry-header">
					<h2 class="entry-title"><?php the_title(); ?></h2>
				</header><!-- .entry-header -->
				<time><?php echo date_format(new DateTime(get_the_date()), 'd/m/y'); ?></time>
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>

						<section>
							<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; ?>
							</article>
						</section>
						<section id="downloads">
							<?php
								if (function_exists('userTypeAttachments')) {
									userTypeAttachments();
								}
							?>
						</section>
						
					<section id="comments">
						<?php //comments_template(); ?>
					</section>

			</div><!-- .span12 -->
		</div><!-- .row-fluid -->
	</div><!-- #content -->
</div><!-- #primary -->		
			

<?php get_sidebar(); ?>
<?php get_footer(); ?>