//ARRAY OF POPUPS
$(window).load(function() {
        centerPopup();
	loadPopup(); 
});
$(document).ready(function() {
  $('.footer-area').append( '<div id="Popup"><a id="popupClose">x</a>This website makes use of new technologies, which your browser may have difficulty in viewing this site in it\'s entirety.<br/>We recommend you update your browser, or install the Google Chrome Framework from here: <a href="http://www.google.com/chromeframe/eula.html?extra=betachannel&hl=en&prefersystemlevel=true">Google Chrome Framework</a></div><div id="bgPopup"></div>');

   $("#bgPopup").data("state",0);
   $("#popupClose").click(function(){
	   	disablePopup();
   });
   
   $(document).keypress(function(e){
		if(e.keyCode==27) {
			disablePopup();	
		}
	});
});

$(window).resize(function() {
centerPopup();
});

function loadPopup(){
	//loads popup only if it is disabled
	if($("#bgPopup").data("state")==0){
		$("#bgPopup").css({
			"opacity": "0.7"
		});
		$("#bgPopup").fadeIn("medium");
		$("#Popup").fadeIn("medium");
		$("#bgPopup").data("state",1);
	}
}
function disablePopup(){
	if ($("#bgPopup").data("state")==1){
		$("#bgPopup").fadeOut("medium");
		$("#Popup").fadeOut("medium");
		$("#bgPopup").data("state",0);
	}
}
function centerPopup(){
	var winw = $(window).width();
	var winh = $(window).height();
	var popw = $('#Popup').width();
	var poph = $('#Popup').height();
	$("#Popup").css({
		"position" : "absolute",
		"top" : winh/2-poph/2,
		"left" : winw/2-popw/2
	});
	//IE6
	$("#bgPopup").css({
		"height": winh	
	});
}


/*


<input type="button" id="myButton" value="Click to activate the Popup!"><div id="Popup"><a id="popupClose">x</a><h1>This is my popup box</h1></div><div id="bgPopup"></div> 




*/