$(document).ready(function(){
 //alert('hello im going to drop a cookie to your pc :) he he he he!');

 // HTML String to write to the cookie agree div
     cookieNameSet = 'agreeToCookies',
     redirect_url = window.location.pathname,
     htmlstring = '<div class="container">'
     htmlstring += '<p class="span2"><strong>Cookies on the United</strong></p>'
     htmlstring += '<p class="span8">We use cookies to ensure that we give you the best experience on our website. <br/>If you continue we will assume that you are happy to receive all cookies on the United House website.</p>'
     htmlstring += '<p class="span2"><button class="btn btn-warning" rel="cookie_agree">Continue</button></p>'
     htmlstring += '</div>'
 
 // Write the string to the page 
 
 //setCookie(cookieNameSet, 'agree', 1);
 
 //checkCookieSetd(cookieNameSet);
 
 
 // call the check cookie function
 if(checkCookieSetd(cookieNameSet) ==true){

      $('div[rel="cookie_agree_box"]').remove();
    } else {

      $('div[rel="cookie_agree_box"]').html(htmlstring);
      $('button[rel="cookie_agree"]').bind('touchstart mousedown', function(){
        setCookie(cookieNameSet, 'agree', 1);
        window.location.replace(redirect_url);
      });
    }
  // add and remove classes from the comments section  
  $('.media-list li').removeClass('comment byuser comment-author-admin even thread-even parent').addClass('media');
  $('.comment-author').removeClass('comment-author vcard').addClass('pull-left');
  $('.comment-reply-link').removeClass('comment-reply-link').addClass('btn btn-info btn-mini');
  $('.comment-reply-login').removeClass('comment-reply-login').addClass('btn btn-info btn-mini');
  $('.form-allowed-tags').remove();
  $('#commentform .form-submit #submit').addClass('btn btn-info');
  $('.widget_archive li').prepend('<i class="icon-file"></i>');
  $('.widget_pages li').prepend('<i class="icon-file"></i>');
  $('.widget_calendar #wp-calendar caption').prepend('<i class="icon-calendar"></i>');
  $('.widget_recent_comments li').prepend('<i class="icon-comment"></i>');
  $('.widget_recent_entries li').prepend('<i class="icon-book"></i>');
  
  
  
  
  
});


// Function to check for the cookie and redirect the page
function checkCookieSetd(name) {  
  if (document.cookie.indexOf(name) >= 0) {
  // They've been here before.
      // console.log('cookie has been already set');
    return true;
  }
    // console.log('cookie has not set');
  return false;
}

// Function to set a cookie
function setCookie(name,value,days) {
    if (days) {
        var date = new Date();
        //7*24*60*60,	
        date.setTime(date.getTime()+(days*7*1000*100*1000));
        var expires = '; expires='+date.toGMTString();
    }
    else var expires = '';
    document.cookie = name+'='+value+expires+'; path=/';
}
