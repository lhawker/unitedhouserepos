<?php
/**
 * Projects ajax call template
 *
 */

?>

	<div id="ajaxContent">		
		
		<?php
			$listAllProjectsByQuery = new ListAllProjectsTaxonomys();	
			
			if(isset($_REQUEST["tax"])):
				$returnedProjects_array = $listAllProjectsByQuery->listProjectsByQuery($_REQUEST["tax"], '', '');
				
				if(!empty($returnedProjects_array)):
					$i = 0;
					$css_rule = 'style="float: left!important;"';
					echo '<section class="projects_cluster">';
					foreach($returnedProjects_array as $obj):
						if($i > 4):
							$i = 1;
							$css_rule = 'style="float: right!important;"';
						else:
							$i = $i + 1;
							if($css_rule == 'style="float: right!important;"'):
								$css_rule = 'style="float: left!important;"';
							endif;
						endif;
						$excerpt = $obj->post_excerpt;
						$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
						$excerpt = strip_tags($excerpt);
						$excerpt = substr($excerpt, 0, 70);
						$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
						$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));	
						
						
						if($i == 1):				
							echo '<article '.$css_rule.' class="projects_cluster_large">';							
							if(get_the_post_thumbnail($obj->ID) != ''):
								$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($obj->ID, 'large'));
								echo  '<span class="projects_cluster_large_img">'.$img_string.'</span>';
							endif;
							echo '<div class="projects_cluster_large_content">';
							echo  '<header><h2>'.$obj->post_title.'</h2></header>';
							echo  '<p>'.$excerpt.' ...</p>';
							echo  '<p><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">Read more ...</a></p>';
							echo '</div>';
							echo '</article>';
						elseif($i > 1):
							echo '<article class="projects_cluster_small">';
							if(get_the_post_thumbnail($obj->ID) != ''):
								$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($obj->ID, 'large'));
								echo  '<span class="projects_cluster_small_img">'.$img_string.'</span>';
							endif;
							echo '<div class="projects_cluster_small_content">';							
							echo  '<header><h2>'.$obj->post_title.'</h2></header>';
							echo  '<p>'.$excerpt.' ...</p>';
							echo  '<p><a href="/projects_ct/'.$obj->post_name.'" title="'.$obj->post_title.'">Read more ...</a></p>';
							echo '</div>';
							echo '</article>';
						endif;
						if($i > 5):
							echo '<div class="clearfix">&nbsp;</div>';
						endif;
					endforeach;
					echo '<div class="clearfix">&nbsp;</div>';
					echo '</section>';				
				endif;
				
			endif;	
			?>
	</div><!-- #ajaxContent -->


