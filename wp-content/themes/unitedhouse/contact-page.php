<?php
/*
 * Template Name: Contact Form Page
 */
	// load header
	get_header();
	///echo do_shortcode('[build-project-category-menu]');
?>
	
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<div class="row-fluid">
			<div class="span12 sm">
		<?php
		// load featured image
		
		$get_map = $wpdb->get_results("SELECT mapid FROM `{$wpdb->prefix}mappress_posts` WHERE postid = $post->ID LIMIT 1");
		
		if(!empty($get_map)){
			echo  '<div class="featured_image"><span class="projects_cluster_large_img">'.do_shortcode('[mappress mapid="17" width="100%" height="380" initialopeninfo="true" directions="false"]').'</span></div>';
		} else {
			if ( has_post_thumbnail() ) {
			$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail());
			echo  '<div class="featured_image"><span class="projects_cluster_large_img">'.$img_string.'</span></div>';
			} 
		}
		
		?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
		<header class="entry-header">
					<h2 class="entry-title"><?php the_title(); ?></h2>
		</header><!-- .entry-header -->
		<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
			</div>
		</div>
		
		
		<div class="row-fluid">
			<div class="span9">
		<section>
			<?php /* The loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					

					<div class="entry-content">
						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
					</div><!-- .entry-content -->
				</article><!-- #post -->

			<?php endwhile; ?>
		</section>
		
		
		<section id="gallery">			
			<?php $gallery = get_post_meta($post->ID, 'select_gallery', true);
				  if(!empty($gallery)):
					$gal = $gallery;
					echo '<h2>Gallery</h2>';
				  	echo do_shortcode('[nggallery id='.$gal.']');
				  endif;
				  
			?>
		</section>
		
		</div><!-- span9 -->
		<div class="span3">
      	<!--Body content-->
			<div class="contact-download-pod">
				<?php
					if (function_exists('userTypeAttachments')) {
						userTypeAttachments();
					}
				?>
			</div>
    	</div>
		</div><!-- .row-fluid -->
		
	</div>
</div>

<?php
	
	
	// load footer
	get_footer();
	
?>

