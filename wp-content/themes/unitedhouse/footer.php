<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
  * @package WordPress
 * @subpackage United_House
 * @since United House 1.0
 */
?>
		</div><!-- #main -->		
	</div><!-- #page -->
	
	<div class="footer-area">
			<div id="page" style="background: transparent;" class="container">
				<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="site-info">
				&copy; Copyright  <?php echo  date('Y').' '.esc_attr( get_bloginfo( 'name', 'display' ) ); ?>
			</div><!-- .site-info -->
			
			<div class="footer_nav_area">
				<nav class="footer_navbar">
					<ul>
						<li><a href="/terms-conditions-of-use" title="Terms &amp; Conditions of use" tabindex="">Terms &amp; Conditions of use</a></li>
						<li><a href="/privacy-policy" title="Privacy Policy" tabindex="">Privacy Policy</a></li>
						<li><a href="/accessibility" title="Accessibility" tabindex="">Accessibility</a></li>
						<li class="footer_navbar_last"><a href="/contact-us" title="Contact Us" tabindex="">Contact Us</a></li>
					</ul>
				</nav>
			</div>
				
			<div class="footer_thumbnail_images">
			<?php 
				$logoOneArray = get_option( 'logoOne' );
				$logoTwoArray = get_option( 'logoTwo' );
				$logoThreeArray = get_option( 'logoThree' );
		
				if(!empty($logoOneArray)) {
					echo '<div class="site_footer_image"><a class="footer_thumbnail" target="_blank" href="'.$logoOneArray['image_url'].'"><img src="'.$logoOneArray['image_upload'].'"/></a></div>';
				}
				if(!empty($logoTwoArray)) {
					echo '<div class="site_footer_image"><a target="_blank" class="footer_thumbnail" href="'.$logoTwoArray['image_url'].'"><img src="'.$logoTwoArray['image_upload'].'"/></a></div>';
				}
				if(!empty($logoThreeArray)) {
					echo '<div class="site_footer_image site_footer_image_large"><a target="_blank" class="footer_thumbnail" href="'.$logoThreeArray['image_url'].'"><img src="'.$logoThreeArray['image_upload'].'"/></a></div>';
				}
			?>
			</div><!-- .footer_thumbnail_images -- >
		</footer><!-- #colophon -->
			</div>
	</div>

	
	
<?php wp_footer(); ?>
<?php
	if(!is_front_page()):
		echo '<p id="back-top"><a href="#top"><span><strong>&#9650</strong></span>Back to top</a></p>';
	endif;
?>

<script type="text/javascript">
var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-17065730-1']);
_gaq.push(['_trackPageview']);
(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();
</script>	
</body>
</html>

	
