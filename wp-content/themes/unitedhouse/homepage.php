<?php
/*
 * Template Name: Homepage / Splash page
 */
get_header(); ?>

<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<?php while ( have_posts() ) : the_post(); ?>
			<nav class="entry-meta">
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
			</nav><!-- .entry-meta -->
				<section>
				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<header class="entry-header">						
						<h2 class="entry-title"><?php the_title(); ?></h2>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php the_content(); ?>
					
						<nav class="section_select_nav">
							<ul rel="selection_menu">
								<li><a class="developer_btn" href="/developer" title="Developer">developer</a></li>
								<li><a class="contractor_btn" href="/contractor" title="Contractor">contractor</a></li>
								<li><a class="refurbisher_btn" href="/refurbisher" title="Refurbisher">refurbisher</a></li>
							</ul>
						</nav>
						<div class="clearfix"></div>
					</div><!-- .entry-content -->
					</article><!-- #post -->
					</section>
					
				
						<?php 
						/*	echo '<section><div class="news_ticker">';
							$excerpt_news_ticker_array = explode("\r", get_the_excerpt());				
							
							echo '<script type="text/javascript">
								  $(document).ready(function() {
    									var num_stories = $(\'.news > span\').length,
    									news_interval = 10000,
    									current_story = 0;
  
  										$(\'.news > span:gt(0)\').hide();
  
  										window.setInterval(function() {  
    										var next_story = (current_story == (num_stories - 1)) ? 0 : current_story + 1;
    										$(\'.news > span:eq(\' + current_story + \')\').hide();
    										$(\'.news > span:eq(\' + next_story + \')\').show();
    										current_story = next_story;
  										}, news_interval);
									});
								  </script>';
							echo '<div class="news">';
							if(!empty($excerpt_news_ticker_array)):
								foreach($excerpt_news_ticker_array as $ticker):
									echo '<span>'.$ticker.'</span>';
								endforeach;
							endif;
							echo '</div>';
						echo '</div></section>';*/
						?>						
				
				
					<section>
					<div class="latest_news">
						<?php
												
							if (class_exists('RandomNews')):
    							$listLatestNewsItems = new RandomNews;
								$newsArray = $listLatestNewsItems->getLatestNews(6);
								//var_dump($listLatestNewsItems->getLatestNews(6));
								$i = 0;
								if(!empty($newsArray)):
									
								echo '<script>
								$(document).ready(function() {
		
									$(\'#Carousel_Latest_News\').carousel({
    									interval: 7000
									});
								});
								</script>';
								echo '<div class="carousel slide" id="Carousel_Latest_News">
											<header>
												<a data-slide="prev" href="#Carousel_Latest_News" class="left">&#9668;</a>
												<h2>Latest News</h2>											
            									<a data-slide="next" href="#Carousel_Latest_News" class="right"> &#9658;</a>
											</header>
										<div class="carousel-inner">';

								foreach($newsArray as $item):
									$i = $i + 1;
									if($i > 1):
										$active_css_class = '';
									else:
										$active_css_class = 'active';
									endif;
									
									echo '<div class="item span12 '.$active_css_class.'">
												<ul class="thumbnails">
                        							<li class="span3">';
												if(get_the_post_thumbnail($item->ID) != ''):
													$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail($item->ID, 'latest-news-home-thumb'));
													echo  '<span class="projects_cluster_large_img">'.$img_string.'</span>';
										endif;
										
										$excerpt = $item->post_excerpt;		
										$excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
										$excerpt = strip_shortcodes($excerpt);
										$excerpt = strip_tags($excerpt);
										$excerpt = substr($excerpt, 0, 220);
										$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
										$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));		
			
									echo			'</li>
													<li class="span8">
														<h3>'.$item->post_title.'</h3>
														<p>'.$excerpt.'... <a href="/'.$item->post_name.'" title="'.$item->post_title.'">Read more</a></p>
													</li>
												</ul>
											</div>';
								endforeach;
								echo		'</div>
									</div>';
								endif;
							endif;
							
						?>
												
						<?php
							
							
						?>
					</div>
					</section>
					
				

			<?php endwhile; ?>
	</div><!-- #content -->
</div><!-- #primary -->
<?php
		$BackgroundCarousel = new BackgroundCarousel();
		$BackgroundCarousel->BackgroundCarouselSelection();			
	?>

<?php get_footer(); ?>