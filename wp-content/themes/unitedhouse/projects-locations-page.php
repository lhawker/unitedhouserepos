<?php
/*
 * Template Name: Project locations page
 */
	// load header
	get_header();
	///echo do_shortcode('[build-project-category-menu]');
?>
<script type="text/javascript">
	// A $( document ).ready() block.
$( document ).ready(function() {

	$('#projects_map').width('100%').height('600px').gmap3({
  		map:{
    		options:{
     			center:[51.607783, -0.109863],
     			zoom: 7,
				styles: [{featureType: 'all',stylers: [{ saturation: -90 },{ lightness: 50 }]}],
     			mapTypeControl: true,
				navigationControl: true,
     			scrollwheel: true,
     			streetViewControl: true
    		}
 		},

  		marker:{
    		values:<?php
				global $post;				
				$post_id = $post->ID;
				$post_type = $post->post_type;
				$post_type = 'projects_ct';
				$post_tax = 'projects';		
				$mapResults = new FindMaps();					
				// call function to return all log-lat from the maps.
				echo $mapResults->getAllMaps($post_id, $post_type, $post_tax);	
			?>,


    
			options:{ draggable: false },

    		events:{
      			click: function(marker, event, context){
        			var map = $(this).gmap3('get'),
          			infowindow = $(this).gmap3({get:{name:'infowindow'}});
        			if (infowindow){
          				infowindow.open(map, marker);
          				infowindow.setContent(context.data);
        			} else {
          				$(this).gmap3({
            				infowindow:{
              					anchor:marker, 
              					options:{content: context.data}
            				}
          				});
        			}
      			},
      			
    		}
  		}
	});
});

</script>
	
<div id="primary" class="content-area page-template-projects-landing-page-php">
	<div id="content" class="site-content" role="main">
		<div class="row-fluid">
			<div class="span12">
			<header class="entry-header">
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header><!-- .entry-header -->
			<!-- map and ajax -->
				<div class="map_area">
					<div id="projects_map">map pleases</div>
						<div class="map_key">
							<ul>
								<li>Map Key:</li>
								<li class="developments_map_icon"><span>&nbsp;</span>Developments</li>
								<li class="contractor_map_icon"><span>&nbsp;</span>Construction</li>
								<li class="refurbishment_map_icon"><span>&nbsp;</span>Refurbishment</li>								
							</ul>
						</div>
				</div>
			<!-- map and ajax -->
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
				<br/>
				<div class="projects_back_btn">
					<a class="btn btn-info" href="/projects" title="Back to Projects">« Back to Projects</a>
				</div>
				<section>	
				
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
				<?php /* The loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<div class="entry-content">
							<?php the_content(); ?>
						</div><!-- .entry-content -->
				</article><!-- #post -->
			<?php endwhile; ?>
			
				</section>
				<section id="downloads">
					<?php
						if (function_exists('userTypeAttachments')) {
							userTypeAttachments();
						}
					?>
				</section>
				
			</div>
		</div>		
	</div>
</div>

<?php
	
	
	// load footer
	get_footer();
	
?>

