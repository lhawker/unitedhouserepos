=== View Own Post Media Only ===
Contributors: shinephp
Donate link: http://www.shinephp.com/donate/
Tags: post, media, view, own, only
Requires at least: 3.2
Tested up to: 3.5
Stable tag: trunk

Limits posts and media library items available for contributors and authors by their own (added, uploaded, attached) only.

== Description ==

This plugin allows to restrict user with Author and Contributor roles to view their own Posts and Media Library items only at admin back-end.
Plugin offers option automatically select "Uploaded to this post" item from drop-down list at Insert Media - Media Library dialog and option to hide this drop-down menu for all users except Administrator and Editor, or other words user with 'edit_other_posts' capability.

== Installation ==

Installing procedure:

1. Deactivate plugin if you have the previous version installed. (It is important requirement for switching to this version from a previous one.)
2. Extract "view-own-posts-media-only.zip" archive content to the "/wp-content/plugins/edit-post-expire" directory.
3. Activate "Edit Post Expire" plugin via 'Plugins' menu in WordPress admin menu. 
4. Plugin has no any settings and works just from the box - activate and forget.

== Screenshots ==
1. screenshot-1.png No other authors items available at Media Library
2. screenshot-2.png No attachment type selection menu at Insert Media window
3. screenshot-3.png Plugin options page

= Translations =
* Russian: [Vladimir Garagulya](http://shinephp.com)

== Frequently Asked Questions ==
- Just ask.


== Changelog ==

= 1.0 =
* 06.01.2013
* 1st release

== Others ==

You can find more information about "View Own Posts Media Only" plugin at this page
http://www.shinephp.com/view-own-posts-media-only-wordpress-plugin/

I am ready to answer on your questions about plugin usage. Use ShinePHP forum at
http://shinephp.com/forums/forum/view-own-posts-media-only
plugin page comments or site contact form for that please.
