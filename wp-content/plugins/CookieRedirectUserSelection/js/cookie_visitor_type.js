var cookieName = 'userselect',
    cookieHomeName = 'homeSelected';
    
$(document).ready(function(){
 //alert('hello im going to drop a cookie to your pc :) he he he he!')
 $('.selection_menu').hide();
 
 if(checkCookieSet(cookieName) !=true){
      $('.selection_menu').slideDown('slow');
    } else if(checkCookieSet(cookieHomeName) !=true){
    }
$('button[rel="show_selection_menu"]').bind('touchstart mousedown', function(){
            
      if($(this).text() == 'User Options +'){
        $(this).text('User Options -');
      } else {
        $(this).text('User Options +');
      }
 
      $('.selection_menu').slideToggle('slow', function() {
          // Animation complete.
        });
  });
    
    $('button[rel="show_selection_menu_button"]').bind('touchstart mousedown', function(){
          if($('button[rel="show_selection_menu"]').text() == 'User Options +'){
              $('button[rel="show_selection_menu"]').text('User Options -');
            } else {
              $('button[rel="show_selection_menu"]').text('User Options +');
            }
            $('.selection_menu').slideToggle('slow', function() {
              // Animation complete.
            });
      });
      
      $('.selection_menu a').bind('touchstart mousedown', function(){
          if($('button[rel="show_selection_menu"]').text() == 'User Options +'){
            $('button[rel="show_selection_menu"]').text('User Options -');
          } else {
            $('button[rel="show_selection_menu"]').text('User Options +');
          }
          $('.selection_menu').slideToggle('slow', function() {
          // Animation complete.
          });
      });
    
    
    // set the users selection cookie for later  
    $('div[rel="selection_menu"] a').bind('touchstart mousedown', function(){
      setCookie(cookieName, $(this).text(), 1);
      unSetCookie(cookieHomeName);
      // reset all user selection cookies
      if(this.rel == 'reset'){
        unSetCookie(cookieName);
      }
    });
    
    $('ul[rel="selection_menu"] li a').bind('touchstart mousedown', function(){
      setCookie(cookieName, $(this).text(), 1);
      unSetCookie(cookieHomeName);
      // reset all user selection cookies
      if(this.rel == 'reset'){
        unSetCookie(cookieName);
      }
    });
    
    $('.site_logo h1 a').bind('touchstart mousedown', function(){
      setCookie(cookieName, $(this).text(), 1);
      unSetCookie(cookieHomeName);
      // reset all user selection cookies
      if(this.rel == 'reset'){
        unSetCookie(cookieName);
        console.log('reset');
      }
    });
    
   
    // set a cookie if the user selects to view the home page so we can override the users option cookie
    $('a[rel="linkhome"]').bind('touchstart mousedown', function(){
      setCookie(cookieHomeName, $(this).text(), 1);
    });

    // we want to remove the home selected cookie when any other a tag is clicked.
    $('a').bind('touchstart mousedown', function(){
      if(this.rel != 'linkhome') {
         unSetCookie(cookieHomeName);
      }
    });
    
    // Back to top actions
    
    // hide #back-top first
	$('#back-top').hide();
	
	// fade in #back-top
	$(function () {
		$(window).scroll(function () {
			if ($(this).scrollTop() > 100) {
				$('#back-top').fadeIn();
			} else {
				$('#back-top').fadeOut();
			}
		});

		// scroll body to 0px on click
		$('#back-top a').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 800);
			return false;
		});
	});
//console.log('btop');

});


// Function to set a cookie
function setCookie(name,value,days) {
    if (days) {
        var date = new Date();
        //2*24*60*60,	
        date.setTime(date.getTime()+(days*2*1000*100*1000));
        var expires = '; expires='+date.toGMTString();
    }
    else var expires = '';
    document.cookie = name+'='+value+expires+'; path=/';
}

// Function to unset a cookie
function unSetCookie(name){
  setCookie(name, null, -1);
}

// Function to get the cookie
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

// Function to check for the cookie and redirect the page
function checkCookieSet(name) {  
  if (document.cookie.indexOf(name) >= 0) {
  // They've been here before.
    //console.log('cookie has been already set');
    return true;
  }
  return false;

}