<?php
	/*
	Plugin URI: http://www.studioeleven.uk.com
	Description: plugin admin options screen.
	Version: 1.0
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
	*/
	
// ******************************************* ADMIN CUSTOM MENU START
class optionsPage{
    public function __construct(){
        if(is_admin()){
	    add_action('admin_menu', array($this, 'add_plugin_page'));
	    add_action('admin_init', array($this, 'page_init'));
	}
    }
	
    public function add_plugin_page(){
    // This page will be under "Settings"
	add_options_page('Cookie Settings Admin', 'Cookie Settings', 'manage_options', 'cookie-setting-admin', array($this, 'create_admin_page'));
    }

    public function create_admin_page(){
        ?>
	<div class="wrap">
	    <?php screen_icon(); ?>
	    <h2>User Cookie Redirect Settings</h2>			
	    <form method="post" action="options.php">
	        <?php
            // This prints out all hidden setting fields
		    settings_fields('cookie_option_group');	
		    do_settings_sections('cookie-setting-admin');
		?>
	        <?php submit_button(); ?>
	    </form>
	</div>
	<?php
    }
	
    public function page_init(){	
	register_setting('cookie_option_group', 'cookie_key', array($this, 'check_ID'));
		
    add_settings_section(
	    'setting_section_id',
	    'Setting',
	    array($this, 'print_section_info'),
	    'cookie-setting-admin'
	);	
		
	add_settings_field(
	    'userOne', 
	    'User one type:', 
	    array($this, 'create_a_field'), 
	    'cookie-setting-admin',
	    'setting_section_id',
		array (
            'id' => 'userOne'
        )			
	);	
	
	add_settings_field(
	    'userOneRediret', 
	    'User one type redirect:', 
	    array($this, 'create_a_field'), 
	    'cookie-setting-admin',
	    'setting_section_id',
		array (
            'id' => 'userOneRediret'
        )
	);	
	
	add_settings_field(
	    'userTwo', 
	    'User two type:', 
	    array($this, 'create_a_field'), 
	    'cookie-setting-admin',
	    'setting_section_id',
		array (
            'id' => 'userTwo'
        )			
	);	
	
	add_settings_field(
	    'userTwoRediret', 
	    'User two type redirect:', 
	    array($this, 'create_a_field'), 
	    'cookie-setting-admin',
	    'setting_section_id',
		array (
            'id' => 'userTwoRediret'
        )
	);
	add_settings_field(
	    'userThree', 
	    'User three type:', 
	    array($this, 'create_a_field'), 
	    'cookie-setting-admin',
	    'setting_section_id',
		array (
            'id' => 'userThree'
        )			
	);	
	
	add_settings_field(
	    'userThreeRediret', 
	    'User three type redirect:', 
	    array($this, 'create_a_field'), 
	    'cookie-setting-admin',
	    'setting_section_id',
		array (
            'id' => 'userThreeRediret'
        )
	);		
		
    }

    public function check_ID($input){
		//$mid = $input['userOne'];
		$mid = $input;
    	if(isset($input)){
	    	if(get_option('cookie_key') === FALSE){
				//add_option('cookie_key', $mid);
	    	}else{
				//update_option('cookie_key', $mid);
	    	}
		}else{
	    	$mid = '';
		}
		return $mid;
    }
	
    public function print_section_info(){
		print 'Enter the cookie setting below. Add a user type and then add the page url,<br/> which the user should be redirected to after the cookie has been dropped:';
    }
	
    public function create_a_field($args){
		$theID = $args['id'];
		$input = get_option('cookie_key');
		// check key in the array		
		if (array_key_exists($theID, $input)) {
			$inputValue = $input[$theID];
		} else {
			$inputValue = '';
		}
		
 
		?>
		<input style="text-transform: lowercase;" type="text" id="<?=$theID;?>" name="<?='cookie_key['.$theID.']' ?>" value="<?=$inputValue;?>" />
		<?php
    }
}

$wctest = new optionsPage();

// ******************************************* ADMIN CUSTOM SETTINGS PAGE END
?>