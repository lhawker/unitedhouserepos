<?php
/*
	Plugin Name: User section and redirect.
	Plugin URI: http://www.studioeleven.uk.com
	Description: This is a plugin that will drop a cookie based on the users selection.
	the next time the user visits the website they will be taken to their selected area of interest.
	Version: 1.0
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
*/
/**************** Disable plugin deactivation for specific plugins */
include_once 'admin/class_options.php';
//echo 'drop this bad boy to the users PC';
function visitor_cookie_script_init() {
	 wp_enqueue_script( 'VisitorCookie', plugins_url( 'js/cookie_visitor_type.js', __FILE__ ) );	
	$userOptionsValues = get_option('cookie_key');
	$htmlString = '';	
	foreach($userOptionsValues as $key => $value){
		if (in_array($key, array('userOne','userTwo','userThree'))) {
			$htmlString .= ' <span class="options_divider">|</span> <a class="optionSelectLink">'.$value.'</a>';
		}
	}	
	 echo '<script type="text/javascript">
				$(document).ready(function(){
					
					$(\'.t1\').live(\'click\', function(){
    				$(\'#yourForm\').trigger(\'submit\');
					});
										
					
					$(\'<div class="userOptions"><button name="show_selection_menu" class="btn btn-mini" rel="show_selection_menu">User Options +</button><div style="margin-bottom: 30px;" rel="selection_menu" class="selection_menu"><a name="reset" rel="reset">Reset selection</a>'.$htmlString.'</div></div>\').prependTo(\'#page\');
					
					$(\'.userOptions\').css({\'position\': \'relative\', \'top\': \'0\', \'right\': \'2px\', \'z-index\': \'2000\', \'text-align\': \'right\'});
					
					$(\'.userOptions a\').css({\'cursor\':\'pointer\'});
					
					$(\'.selection_menu\').css({\'margin-top\':\'2px\', \'position\':\'absolute\', \'top\': \'-2px\', \'right\': \'86px\', \'padding\': \'9px 10px\',\'-webkit-border-radius\': \'5px 0 5px 5px\',\'-moz-border-radius\': \'5px 0 5px 5px\',\'border-radius\': \'5px 0 5px 5px\'});
								
					$(\'.optionSelectLink\').each(function () {
      					var el = $(this),
            			text = el.html(),
            			first = text.slice(0, 1),
            			rest = text.slice(1);
      					el.html(\'<span class="firstletter">\' + first + \'</span>\' + rest);
					});
					
					$(\'.firstletter\').css({\'text-transform\': \'uppercase\'});					
				});
		  </script>';
}
add_action( 'wp_head', 'visitor_cookie_script_init', 10);
if (isset($_COOKIE["userselect"])){	
	$cookieValue = $_COOKIE["userselect"];	
	$userOptionsValues = get_option('cookie_key');
	//echo $_SERVER["SERVER_NAME"].'<br/>'; // << testing only
	//echo $_SERVER["REQUEST_URI"].'<br/>'; // << testing only
	foreach($userOptionsValues as $key => $value){
		if (in_array($key, array('userOne','userTwo','userThree'))) {
			if($value == $cookieValue){
				if($_SERVER["REQUEST_URI"] == '/'){
					//echo $userOptionsValues[$key.'Rediret'].'<br/>'; // << testing only
					$path =	$_SERVER["SERVER_NAME"].$userOptionsValues[$key.'Rediret'];
					header( 'Location: http://'.$path.'' ) ;
					exit;	
				}
			}
		}
	}
}
?>