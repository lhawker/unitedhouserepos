<?php
/*
	Plugin Name: Custom Admin Bar
	Plugin URI: http://www.studioeleven.uk.com
	Description: This is a plugin will change the admin menu bar.
	Version: 1.0
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
*/
/**************** Disable plugin deactivation for specific plugins */
add_action( 'load-index.php', 'hide_welcome_panel' );

function hide_welcome_panel() {
    $user_id = get_current_user_id();

    if ( 1 == get_user_meta( $user_id, 'show_welcome_panel', true ) )
        update_user_meta( $user_id, 'show_welcome_panel', 0 );
}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets');

function remove_dashboard_widgets () {
      //Completely remove various dashboard widgets (remember they can also be HIDDEN from admin)
      remove_meta_box( 'dashboard_quick_press',   'dashboard', 'side' );      //Quick Press widget     
      remove_meta_box( 'dashboard_primary',       'dashboard', 'side' );      //WordPress.com Blog
      remove_meta_box( 'dashboard_secondary',     'dashboard', 'side' );      //Other WordPress News
      remove_meta_box( 'dashboard_incoming_links','dashboard', 'normal' );    //Incoming Links
      remove_meta_box( 'dashboard_plugins',       'dashboard', 'normal' );    //Plugins
}

function new_custom_login_logo() {
	echo '<style type="text/css">
		h1 a { background-image: url('.plugins_url().'/CustomAdminBar/images/logo_admin_login.png) !important; height:130px !important; background-size: auto auto !important; height: 80px!important; }
		</style>';
}
add_action('login_head', 'new_custom_login_logo');

function new_wp_login_url() {
	return home_url();
}
add_filter('login_headerurl', 'new_wp_login_url');

function new_wp_login_title() {
	return get_option('blogname');
}
add_filter('login_headertitle', 'new_wp_login_title');

function new_custom_login_page_style() {
	echo '<style type="text/css">
		body.login    { background-color: #f5f5f5; }
		#loginform, #lostpasswordform { background-color: #194272; }
		#loginform, #lostpasswordform label {color: #FFF ;}
		#backtoblog {display: none;}
		</style>';
}

add_action('login_head', 'new_custom_login_page_style');


function custom_admin_script_init() {
	 wp_enqueue_script( 'CustomAdminScript', plugins_url( 'js/change_link_titles.js', __FILE__ ) );
}
function custom_admin_style_init() {
   /* Register our stylesheet. */
   wp_register_style( 'CustomAdminStylesheet', plugins_url('css/admin_bar.css', __FILE__) );
   wp_enqueue_style( 'CustomAdminStylesheet' );
}


add_action('admin_print_scripts', 'custom_admin_script_init', true);
add_action( 'admin_init', 'custom_admin_style_init', false);
add_action( 'wp_head', 'custom_admin_style_init', false);

function change_post_object_label() {
	global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News &amp; Blogs';
    $labels->singular_name = 'News &amp; Blogs';
    $labels->add_new = 'Add News &amp; Blogs';
    $labels->add_new_item = 'Add News &amp; Blogs';
    $labels->edit_item = 'Edit News &amp; Blogs';
    $labels->new_item = 'News &amp; Blogs';
    $labels->view_item = 'View News &amp; Blogs';
    $labels->search_items = 'Search News &amp; Blogs';
    $labels->not_found = 'No News &amp; Blogs found';
    $labels->not_found_in_trash = 'No News &amp; Blogs found in Trash';
}
add_action( 'init', 'change_post_object_label' );

function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News &amp; Blogs';
    $submenu['edit.php'][5][0] = 'News &amp; Blogs';
    $submenu['edit.php'][10][0] = 'Add News &amp; Blogs';
}

add_action( 'admin_menu', 'change_post_menu_label' );

function custom_admin_bar_render() {
	global $wp_admin_bar;
        $wp_admin_bar->add_menu( array(
        'parent' => 'new-content',
        'id' => 'projects',
        'title' => __('Add Project'),
        'href' => admin_url( 'post-new.php?post_type=projects_ct')
    ) );
	$wp_admin_bar->add_menu( array(
        'parent' => 'new-content',
        'id' => 'new-post',
        'title' => __('Add News'),
        'href' => admin_url( 'post-new.php')
    ) );
	$wp_admin_bar->add_menu( array(
        'parent' => 'new-content',
        'id' => 'new-media',
        'title' => __('Add Media'),
        'href' => admin_url( 'media-new.php')
    ) );
	$wp_admin_bar->add_menu( array(
        'parent' => 'new-content',
        'id' => 'new-page',
        'title' => __('Add Page'),
        'href' => admin_url( 'post-new.php?post_type=page')
    ) );
}
add_action( 'wp_before_admin_bar_render', 'custom_admin_bar_render' );


function my_remove_menu_pages() {
	global $current_user;
	if ($current_user->roles[0] == 'hr_team' ):
		remove_menu_page('edit.php');
		remove_menu_page('tools.php');
		remove_menu_page('edit-comments.php');
		remove_menu_page('profile.php');
		
		
	endif;	
}

add_action( 'admin_menu', 'my_remove_menu_pages' );
// items to remove
function remove_admin_bar_items( $wp_admin_bar ) {
	global $current_user;
	
    $wp_admin_bar->remove_node('wp-logo');
	$wp_admin_bar->remove_node('ngg-menu');
	$wp_admin_bar->remove_node('new-link');
	$wp_admin_bar->remove_node('new-user');
	
	
	if ($current_user->roles[0] == 'hr_team' ):
		$wp_admin_bar->remove_node('new-content');
		$wp_admin_bar->remove_node('comments');
		$wp_admin_bar->remove_node('gallery_menu');

	endif;
	
}

add_action( 'admin_bar_menu', 'remove_admin_bar_items', 999 );


//
function add_room_admin_bar_link() {
	global $wp_admin_bar, $current_user;

	if ( $current_user->user_ID == 3 || !is_admin_bar_showing() )
		return;
	$wp_admin_bar->add_menu( array(
	'id' => 'gallery_menu',
	'title' => __( 'Gallery'),
	'href' => admin_url( 'admin.php?page=gallery')
	) );
	
	$wp_admin_bar->add_menu( array(
        'parent' => 'gallery_menu',
        'id' => 'gallery-add-image',
        'title' => __('Add Gallery Images'),
        'href' => admin_url( 'admin.php?page=nggallery-add-gallery')

    ) );

	$wp_admin_bar->add_menu( array(
        'parent' => 'gallery_menu',
        'id' => 'gallery-add-gallery',
        'title' => __('Manage Galleries'),
        'href' => admin_url( 'admin.php?page=nggallery-manage-gallery')

    ) );
	
	$wp_admin_bar->add_menu( array(
	'id' => 'hr_team',
	'title' => __( 'HR Team Content')
	) );
		
	$wp_admin_bar->add_menu( array(
        'parent' => 'hr_team',
        'id' => 'hr-page',
        'title' => __('Add HR page'),
        'href' => admin_url( 'post-new.php?post_type=page')

    ) );
	$wp_admin_bar->add_menu( array(
        'parent' => 'hr_team',
        'id' => 'hr-new-form',
        'title' => __('Add application form'),
        'href' => admin_url( 'admin.php?page=ninja-forms&tab=form_settings&form_id=new')

    ) );
	$wp_admin_bar->add_menu( array(
        'parent' => 'hr_team',
        'id' => 'hr-all-form',
        'title' => __('View all current forms'),
        'href' => admin_url( 'admin.php?page=ninja-forms')

    ) );
	$wp_admin_bar->add_menu( array(
        'parent' => 'hr_team',
        'id' => 'hr-form-subs',
        'title' => __('View application submissions'),
        'href' => admin_url( 'admin.php?page=ninja-forms-subs')

    ) );


}
add_action('admin_bar_menu', 'add_room_admin_bar_link',100);

// Add a custom admin widget

// Create the function to output the contents of our Dashboard Widget

function form_dashboard_widget_function() {
	// Display whatever it is you want to show
	echo "<p><strong>Export a CSV of the most recent form submissions below:</strong></p>";
	global $wpdb;
	
	//$queryFormsSubs ="SELECT * FROM `{$wpdb->prefix}ninja_forms_subs` ORDER BY date_updated LIMIT 10";
	
	$queryFormsSubs ="SELECT a.id, a.form_id, a.data, a.date_updated, b.data FROM `{$wpdb->prefix}ninja_forms_subs` AS a LEFT JOIN `{$wpdb->prefix}ninja_forms` AS b ON a.form_id = b.id ORDER BY a.date_updated LIMIT 10;";
	$data = $wpdb->get_results($queryFormsSubs);
	
	if(!empty($data)){
		echo '<table border="0" width="100%">';
		foreach($data as $key => $value){
			echo '<tr>';
			$objData = unserialize($value->data);
			echo '<th width="50%" align="left">Submissions added to : <a href="http://www.wdemo.co.uk/wp-admin/admin.php?page=ninja-forms-subs&edit_sub_form=1&sub_id='.$value->id.'&form_id='.$value->form_id.'">'.$objData['form_title'].'</a></th>';
			//echo '<td>'.substr( $objData['admin_email_msg'], 0, 5500 ).'...</td>';
			echo '<td width="25%" align="center"> <a href="http://www.wdemo.co.uk/wp-admin/admin.php?page=ninja-forms-subs&edit_sub_form=1&sub_id='.$value->id.'&form_id='.$value->form_id.'">view / edit</a></td>';
			echo '<td width="25%" align="left"><a href="http://www.wdemo.co.uk/wp-admin/admin.php?page=ninja-forms-subs&ninja_forms_export_subs_to_csv=1&sub_id='.$value->id.'&form_id='.$value->form_id.'">export</a></td>';
			echo '</tr>';
		}
		echo '</table>';
	}
	
	
	
} 

// Create the function use in the action hook

function form_add_dashboard_widgets() {
	wp_add_dashboard_widget('form_dashboard_widget', 'Recent Form Submissions', 'form_dashboard_widget_function');
	
	global $wp_meta_boxes;
	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
	$form_widget_backup = array('form_dashboard_widget' => $normal_dashboard['form_dashboard_widget']);
	unset($normal_dashboard['form_dashboard_widget']);
	$sorted_dashboard = array_merge($form_widget_backup, $normal_dashboard);
	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;	
} 

add_action('wp_dashboard_setup', 'form_add_dashboard_widgets' );

?>