jQuery(document).ready(function($) {	
  
function updateTextArea() {
  
  if ($(this).is(':checked')) {
      $(this).attr("checked", "checked");
      $(this).val("yes");
      //console.log($(this));
  } else {
      $(this).removeAttr("checked");
      $(this).val("no");
      //console.log($(this));
  }
}
 
$(function() {
  $('#c_b input').click(updateTextArea);
  // $('#the-list input[type=checkbox]').click(updateTextArea);
   //updateTextArea();
});
 
});