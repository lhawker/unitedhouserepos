<?php 
/***************************************************************************************/
//	This include file contains the banner ajax load JQ
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/**************************************************************************************
*/
global $post, $wpdb;
// Function to search the posts content to find a matching shortcode.
	function match_shortcode(){
		global $post;
		$content_string = $post->post_content;
		$re1 = '/\[autoload-banner banner_id=\"([0-9]+)\"]/e';
		$count = preg_match_all($re1, $content_string, $matches);	
		if($count == TRUE):
			return $matches[1][0];
		else:
			return FALSE;
		endif;
	}
	
// Function to get the post obj from the db and give context to the template called by ajax
	function get_post_obj(){
		global $wpdb;
		if(match_shortcode() == TRUE):
			$banner_id = match_shortcode();
			$post_status_array = array();
			array_push($post_status_array, "'publish', 'draft'");	
			$post_status_str = implode(", ", $post_status_array);			
			$banner_results = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}posts` WHERE ID =$banner_id AND post_status IN ($post_status_str)");
			//return $banner_results[0]->guid;
			// build the URL	
					
			return substr(str_replace("post_type=", "", $banner_results[0]->guid), 0, strpos(str_replace("post_type=", "", $banner_results[0]->guid), "&")).'='.str_replace(" ", "-", $banner_results[0]->post_name);
			
		else:
			return FALSE;
		endif;
	}

// Function to get the element selector from the banner settings
	function get_banner_settings(){
		global $wpdb;
		if(match_shortcode() == TRUE):
			$option_value = $wpdb->get_results("SELECT option_value FROM `{$wpdb->prefix}options` WHERE option_name = 'selector_value'");
			return $option_value[0]->option_value;
		else:
			return FALSE;
		endif;
	}

// Output the jQuery which loads the banner into the selector.
echo '<script type="text/javascript">
		jQuery(document).ready(function($) {    		
			var request = $.ajax({
  				url: "'.get_post_obj().'",
  				type: "GET",
  				dataType: "html"
			});
			request.done(function(msg) {
  				$("'.get_banner_settings().'").html( msg );
			});
			
			request.fail(function(jqXHR, textStatus) {
  				//alert( "Request failed: " + textStatus );
			});
			
		});
	 </script>';
?>