<?
/***************************************************************************************/
//	This include file contains the functions to add extra page select fields to the this 
//	field will show all the page / posts within the CMS which the banner may be dispalyed 
//	on.
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/
// Globals, build list of mime types as a string to use within the db query
$page_type_array = array();
array_push($page_type_array, "'page', 'post'");	
$page_type_str = implode(", ", $page_type_array);
$short_code_tag = 'autoload-banner';

// ******************************************* Add Custom Meta-Boxs START
// 1. Add Custom Meta-Boxs
add_action( 'admin_init', 'create_attach_meta' );

function create_attach_meta() {
	$priority = 'high';
	add_meta_box('custom_attach_meta_fields', 'Available Pages To Attach The Banner To.', 'custom_attach_meta_fields', 'banner_autoload_type'); 
}
// ******************************************* Add Custom Meta-Boxs END


// ******************************************* Add function to find all the pages and posts in the database STRAT
function get_page_post() {
	global $wpdb, $page_type_array, $page_type_str;
	$showresults = $wpdb->get_results("SELECT ID, post_title, guid, post_status FROM `{$wpdb->prefix}posts` WHERE post_type IN ($page_type_str) AND post_status = 'publish'");
	return $showresults;
	$wpdb->flush();	
}

// ******************************************* Add function to find all the pages and posts in the database

// ******************************************* Add class function search for keys within an array START
$class_directory = WP_PLUGIN_DIR.'/'.str_replace(basename( __FILE__),"",str_replace('fields', 'classes',plugin_basename(__FILE__)));
include_once $class_directory.'search_keys_class.php';
// ******************************************* Add class function search for keys within an array END

// ******************************************* Add Custom Fields START
// Attachment Reference field added
function custom_attach_meta_fields() {
// - grab data -
    global $post, $wpdb, $post_ID, $short_code_tag;
	$post_id = get_page_post();
	$custom_meta_id = get_post_custom($post->ID);
	$post_ID = $post->ID;
	$diff_array = array();
	
	if (isset($custom_meta_id["selected_pages"][0])):
		$meta_array = unserialize($custom_meta_id["selected_pages"][0]);
	else:
		$meta_array = array();
	endif;

	echo '<div class="meta_img">
		  <table class="wp-list-table widefat fixed media" cellspacing="0">
		  <thead>
			<tr>
				<th scope="col" id="cb" class="manage-column column-cb check-column">&nbsp;Title</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Selected</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Page URL</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Page Status</th>
		  	</tr>
		  <thead>
		  <tfoot>
			<tr>
				<th scope="col" id="cb" class="manage-column column-cb check-column">&nbsp;Title</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Selected</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Page URL</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Page Status</th>
		  	</tr>
		  <tfoot>
		  <tbody id="the-list">';
	
	foreach (get_page_post() as $post):
		echo '<tr>';
		echo '<td class="title column-title"><div id="icon-edit-pages" class="icon32 icon32-posts-page"><br></div><strong>'.str_replace('_', ' ', $post->post_title).'</strong></td>';
		$post_id = $post->ID;
		// check to see if the image id is already saved to the db
		if(is_array($meta_array)):
		else:
			$meta_array = array();
		endif;
		
		array_push($diff_array, array_diff(array($post_id), $meta_array));	
		
		if (in_array($post_id, $meta_array)):
			$newSearchKeyObject = new Search_Keys($array,$key);
			echo '<td class="title column-title">
				  	<input type="checkbox" name="selected_pages[]" value="'.$post_id.'" checked="checked" />
				  </td>';		
			echo '<td>'.$post->guid.'</td>';
		else:
			echo '<td class="title column-title">
				  <input type="checkbox" name="selected_pages[]" value="'.$post_id.'" />
				  </td>';
			echo '<td>'.$post->guid.'</td>';
		endif;
		echo '<td class="title column-title"><strong class="'.$post->post_status.'">'.str_replace('_', ' ', $post->post_status).'</strong></td>';
		echo '</tr>';	
	endforeach;
	$i =0;
		foreach($diff_array as $obj):
		// If the item has not been selected then remove the shortcode and ID		
			if(isset($obj[0])):
				$content_sc_from = '['.$short_code_tag.' banner_id="'.$post_ID.'"]';
				$content_sc_to = '';
				$wpdb->query( $wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = REPLACE(post_content, %s, %s) WHERE ID= $obj[0]", $content_sc_from, $content_sc_to)); 
				$wpdb->flush();
			endif;
		endforeach;
    // - convert to pretty formats -
	echo '</tbody></table></div>';
	if (isset($_POST["selected_pages"])):
	echo 'heeeeee';
	else:	
		//echo '<input type="checkbox"  name="selected_pages[]" value="set" checked="checked"/>';
	endif;
} 

// ******************************************* Add Custom Fields END

// ******************************************* Save Fields Meta data START
add_action ('save_post', 'save_selected_meta_fields');

function save_selected_meta_fields(){
	global $post, $wpdb, $page_type_array, $page_type_str, $short_code_tag;
	
	if (isset($_POST["selected_pages"])):
	foreach($_POST["selected_pages"] as $items):	

			$showresults = $wpdb->get_results("SELECT * FROM `{$wpdb->prefix}posts` WHERE ID =$items AND post_status = 'publish' AND post_type IN ('page', 'post')");	
			
				

			$clean_content = preg_replace ("/\[autoload-banner banner_id=\"([0-9]+)\"]/e", "", $showresults[0]->post_content);
			$wpdb->update( $wpdb->posts, array( 'post_content' => '['.$short_code_tag.' banner_id="'.$post->ID.'"] '.$clean_content ),array( 'ID' => $items ));
			$wpdb->flush();
			
		
	endforeach;
	
	// Update the ftc_reg_form
	update_post_meta($post->ID, "selected_pages", $_POST["selected_pages"] );
	if ( !current_user_can( 'edit_post', $post->ID )):
        	return $post->ID;
	endif;
	else:
		if(!isset($post)):			
		else:
			if($post->post_type == 'banner_autoload_type'):				
				// A way of removing all instances of [autoload-banner banner_id] shortcade when we delete the items.
				$content_sc_from = '['.$short_code_tag.' banner_id="'.$post->ID.'"]';
				$content_sc_to = '';
				$wpdb->query( $wpdb->prepare("UPDATE {$wpdb->posts} SET post_content = REPLACE(post_content, %s, %s)", $content_sc_from, $content_sc_to));
				$wpdb->flush();
				delete_post_meta($post->ID, 'selected_pages');
			endif;
	
		endif;
	endif;
		
}
	
// ******************************************* Save Fields Meta data END
?>