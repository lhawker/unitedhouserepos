<?
/***************************************************************************************/
//	This include file contains the functions to add extra fields to the 
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/


// ******************************************* LOAD ADMIN JAVASCRIPT START
function checkbox_scripts(){
	wp_enqueue_script('custom_script', trailingslashit(WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",str_replace('/fields', '', 	plugin_basename(__FILE__)))) . 'js/checkbox.js', array('jquery'));
}
add_action( 'admin_print_scripts-post.php', 'checkbox_scripts', 1000 );
add_action( 'admin_print_scripts-post-new.php', 'checkbox_scripts', 1000 );
add_action('template_redirect', 'checkbox_scripts');
// ******************************************* LOAD ADMIN JAVASCRIPT END

function metabox_styles(){
	global $post_type;
	if( $post_type == 'banner_autoload_type'):
		wp_enqueue_style('metabox_styles', trailingslashit(WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",str_replace('/fields', '', plugin_basename(__FILE__)))) . 'css/admin_metaboxes.css');
	endif;
}
add_action( 'admin_print_styles-post.php', 'metabox_styles', 1000 );
add_action( 'admin_print_styles-post-new.php', 'metabox_styles', 1000 );


// ******************************************* LOAD ADMIN STYLES START
// ******************************************* LOAD ADMIN STYLES START

// ******************************************* Add Custom Meta-Boxs START
// 1. Add Custom Meta-Boxs
add_action( 'admin_init', 'create_image_meta' );

function create_image_meta() {
	$priority = 'high';
	add_meta_box('custom_image_meta_fields', 'Available Banner Images', 'custom_image_meta_fields', 'banner_autoload_type'); 
}
// ******************************************* Add Custom Meta-Boxs END
// ******************************************* Add function to find all the images in the database STRAT
function get_images() {
	global $wpdb;
	$mime_type_array = array();
	array_push($mime_type_array, "'image/gif', 'image/jpeg', 'image/png'");
	// build list of mime types as a string to use within the db query
	$mime_type_str = implode(", ", $mime_type_array);	
	$showresults = $wpdb->get_results("SELECT ID, post_title, guid FROM `{$wpdb->prefix}posts` WHERE post_type = 'attachment' AND post_mime_type IN ($mime_type_str)");
	return $showresults;	
}
// ******************************************* Add function to find all the images in the database END

// ******************************************* Add class function search for keys within an array START
$class_directory = WP_PLUGIN_DIR.'/'.str_replace(basename( __FILE__),"",str_replace('fields', 'classes',plugin_basename(__FILE__)));
include_once $class_directory.'search_keys_class.php';
// ******************************************* Add class function search for keys within an array END

// ******************************************* Add Custom Fields START
// Image Reference field added
function custom_image_meta_fields() {
// - grab data -
    global $post;
	$image_id = get_images();
	$custom_meta_id = get_post_custom($post->ID);
	
	if (isset($custom_meta_id["selected_banner"][0])):
		$meta_array = unserialize($custom_meta_id["selected_banner"][0]);
	else:
		$meta_array = array();
	endif;
//<input type="text" id="first_field" name="field[1][points]" /><br />
	echo '<div class="meta_img">
		  <table class="wp-list-table widefat fixed media" cellspacing="0">
		  <thead>
			<tr>
		  		<th scope="col" id="cb" class="manage-column column-cb check-column"><label for="selected_banner">&nbsp;Banner Images</label></th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Title</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Selected</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Clients Name</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Link URL</th>
		  	</tr>
		  <thead>
		  <tfoot>
			<tr>
		  		<th scope="col" id="cb" class="manage-column column-cb check-column"><label for="selected_banner">&nbsp;Banner Images</label></th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Title</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Selected</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Clients Name</th>
				<th scope="col" id="cb" class="manage-column column-cb check-column">Link URL</th>
		  	</tr>
		  <tfoot>
		  <tbody id="the-list">';
	
	foreach (get_images() as $image):
		echo '<tr>';
		echo '<td class="column-icon media-icon">'.wp_get_attachment_image( $image->ID, 'thumbnail').'</td>';
		echo '<td class="title column-title"><strong>'.str_replace('_', ' ', $image->post_title).'</strong></td>';
		$image_id = $image->ID;
		// check to see if the image id is already saved to the db
		if (in_array($image_id, $meta_array)):
			echo '<td class="title column-title">
				  	<input type="checkbox" name="selected_banner['.$image_id.'_ID]" value="'.$image_id.'" checked="checked" />
				  </td>';
				$newSearchKeyObject = new Search_Keys($array,$key);
				
			echo '<td><input type="text" name="selected_banner['.$image_id.'_client]" value="'.$newSearchKeyObject->get_value_by_key($meta_array, $image_id.'_client').'" /></td>';
			echo '<td><input type="text" name="selected_banner['.$image_id.'_url]" value="'.$newSearchKeyObject->get_value_by_key($meta_array, $image_id.'_url').'" /></td>';
		else:
			echo '<td class="title column-title">
				  <input type="checkbox" name="selected_banner['.$image_id.'_ID]" value="'.$image_id.'" />
				  </td>';
			echo '<td><input type="text" name="selected_banner['.$image_id.'_client]" value="" /></td>';
			echo '<td><input type="text" name="selected_banner['.$image_id.'_url]" value="" /></td>';
		endif;
		echo '</tr>';	
	endforeach;
    // - convert to pretty formats -
	echo '</tbody></table></div>';
} 

// ******************************************* Add Custom Fields END

// ******************************************* Save Fields Meta data START
add_action ('save_post', 'save_image_meta_fields');

function save_image_meta_fields(){
	global $post;
	
	// if all gos ok I'll remove this
	/*if(isset($_POST['selected_banner'])):
		$aDoor = $_POST['selected_banner'];
	endif;	
	
  	if(empty($aDoor)) {
    	//echo("You didn't select any buildings.");
  	} else {
    	$N = count($aDoor);
    	for($i=0; $i < $N; $i++):
      		echo($aDoor[$i] . " ");
			//update_post_meta($post->ID, "$aDoor[$i]", $aDoor[$i] );
			update_post_meta($post->ID, "selected_banner", $aDoor );
    	endfor;
  	}*/
		
	if (isset($_POST["selected_banner"])) {
		// Update the ftc_reg_form
        update_post_meta($post->ID, "selected_banner", $_POST["selected_banner"] );
		if ( !current_user_can( 'edit_post', $post->ID ))
        	return $post->ID;
	}
}
// ******************************************* Save Fields Meta data END	
?>