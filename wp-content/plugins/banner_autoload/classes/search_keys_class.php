<?php
/***************************************************************************************/
//	This include file contains search array keys class
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/
// thanks to http://stackoverflow.com/questions/4467071/php-search-key-in-array-and-return-its-value // remove this when finished
class Search_Keys {
	function get_value_by_key($array,$key) {		
		foreach($array as $k=>$each):
			if($k==$key):
				return $each;
  			endif;

  			if(is_array($each)):
				//if($return = get_value_by_key($each,$key)):
				if($return = $this->get_value_by_key($each,$key)):
					return $return;
   				endif;
  			endif;
 		endforeach;
	}
}

?>