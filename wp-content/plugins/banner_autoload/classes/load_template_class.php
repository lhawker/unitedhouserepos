<?
/***************************************************************************************/
//	This include file contains the load template classes
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/


/**************** Class to locate templates from within the plugin directory *************/
class Locate_Templates {
}
// call to class add_filter('single_template', array('Locate_Templates', 'get_custom_pt_templated'));


/**************** *************/
/**************** Function to be able to locate templates from within the plugin directory *************/
function couresware_plugin_template($template_names, $load = false, $require_once = true ) {
	//echo 'templates';
    if ( !is_array($template_names) )
        return '';
		$located = '';
    	//$kthis_plugin_dir = WP_PLUGIN_DIR.'/'.str_replace( basename( __FILE__), "", plugin_basename(__FILE__) ); 
		$this_plugin_dir = WP_PLUGIN_DIR.'/'.str_replace(basename( __FILE__),"",str_replace('/classes', '', plugin_basename(__FILE__)) . 'templates/' ); 
	//	echo $this_plugin_dir;
    foreach ( $template_names as $template_name ) {

        if ( !$template_name )
            continue;
        if ( file_exists(STYLESHEETPATH . '/' . $template_name)) {
            $located = STYLESHEETPATH . '/' . $template_name;
            break;
        } else if ( file_exists(TEMPLATEPATH . '/' . $template_name) ) {
            $located = TEMPLATEPATH . '/' . $template_name;
            break;
        } else if ( file_exists( $this_plugin_dir .  $template_name) ) {
            $located =  $this_plugin_dir . $template_name;
            break;
        }
    }    
    if ( $load && '' != $located )
        load_template( $located, $require_once );    
    return $located;
}


// Apply the custom tempage to the part-time post type
add_filter( 'single_template', 'get_custom_pt_template' );
/**************** Function for single template *************/
function get_custom_pt_template($template) {
	global $wp_query;
    $object = $wp_query->get_queried_object();
    if ( $object->post_type == 'banner_autoload_type') {
        $templates = array('single-' . $object->post_type . '.php', 'single.php');
        $template = couresware_plugin_template($templates);
    }
	//Array ( [0] => single-banner_autoload_type.php [1] => single.php )
    return $template;
}


?>