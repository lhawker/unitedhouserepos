<?php
/***************************************************************************************/
//	This include file loades all the CSS and javascript for the plugin
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/

// Styles
function load_css() {
	wp_enqueue_style('ab-css', trailingslashit(WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",str_replace('/classes', '', plugin_basename(__FILE__)))) . 'css/banner_element.css');
}
add_action('wp_enqueue_scripts', 'load_css');

// ******************************************* LOAD JAVASCRIPT START

function include_banner(){
// load javascript
include_once trailingslashit(WP_PLUGIN_DIR.'/'.str_replace(basename( __FILE__),"",str_replace('/classes', '', plugin_basename(__FILE__)))) . 'js/include_banner.js.php';
}
add_action('wp_head', 'include_banner');
// ******************************************* LOAD JAVASCRIPT END

?>