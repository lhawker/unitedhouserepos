<?php
/***************************************************************************************/
//	This include file contains shortcode class
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/
class ABShortCode {
	public function autoload_banner($atts) {
	global $post;		
	if (!empty($atts)):
		$this_plugin_dir = WP_PLUGIN_DIR.'/'.str_replace(basename( __FILE__),"",str_replace('/classes', '', plugin_basename(__FILE__)) . 'templates/' ); 
		$selected_banner = get_post_meta($atts['banner_id'], 'selected_banner', true);
		// If you would like to add functionality to be displayed within the content area then add it below.
		//include_once $this_plugin_dir.'single-banner_autoload_type.php';	
	endif;	
	}
}
add_shortcode( 'autoload-banner', array('ABShortCode', 'autoload_banner') ); 
?>