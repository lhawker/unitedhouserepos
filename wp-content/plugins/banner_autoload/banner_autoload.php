<?php
/*
Plugin Name: Banner autoload
Plugin URI: http://geansai.co.uk
Description: This is a plugin to add a new custom banner post-type to Wordpress. This Banner autoload product will allow to creat a banner page, which the user can upload as many images as they like for the banner to display. The banner autoload will then display these banner images as a Jq carousel. The Banner autoload product will also allow the editor to select the pages they wish the banner to be displayed on. The editor will need to use the banner autoload settings to add the #id name of the element within the template they wish the banner to load into.

Version: 1.0
Author: Geansai .Ltd
Author URI: http://geansai.co.uk/Banner autoload
License: GPLv2 or later
*/

/*
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
// load templates
include_once 'classes/load_template_class.php';

// load shortcodes
include_once 'classes/short_code_class.php';

// load css and javascripts
include_once 'classes/load_js_css.php';



/*$LocateTemplates = new Locate_Templates();


$LocateTemplates->couresware_plugin_template($template_names, $load = false, $require_once = true);*/

// ******************************************* POST TYPES START
// 1. Post Type Registration (banner autoload page-type)
add_action('init', 'banner_autoload_type');
function banner_autoload_type() {
	$labels = array(
    	'name' => _x('Banner', 'post type general name'),
    	'singular_name' => _x('Banner', 'post type singular name'),
    	'add_new' => _x('Add New Banner', 'Banner'),
    	'add_new_item' => __('Add New Banner'),
    	'edit_item' => __('Edit Banner'),
    	'new_item' => __('New Banner'),
    	'view_item' => __('View Banner'),
    	'search_items' => __('Search Banners'),
    	'not_found' =>  __('No Banners found'),
    	'not_found_in_trash' => __('No Banners found in Trash'),
    	'parent_item_colon' => '',
	);
 	$banner_args = array(
     	'labels' => $labels,
     	'singular_label' => __('Banner'),
     	'public' => true,
     	'can_export' => true,
    	'show_ui' => true,
		'show_in_menu' => 'banner_autoload',
		'exclude_from_search' => true,
     	'capability_type' => 'page',
     	'hierarchical' => false,
     	'rewrite' => true,
     	'supports' => array('title', 'excerpt'),
		'taxonomies' => array('banner_autoload')
     );
 	register_post_type('banner_autoload_type',$banner_args);
}

// Add functions to add custom meta fields to the news post type and save the meta data on save
include_once 'fields/image_field.php';
include_once 'fields/page_select_field.php';

// ******************************************* ADMIN CUSTOM MENU START
add_action('admin_menu', 'homepage_create_admin_menu');
function homepage_create_admin_menu() {
    add_menu_page('Banner Autoload', 'Banner Autoload', 'edit_users', 'banner_autoload', '',plugins_url('/images/ab_icon.gif', __FILE__),31,'');
	add_submenu_page('banner_autoload','Autoload Settings', 'Autoload Settings', 'manage_options', __FILE__, 'banner_autoload_settings_page',plugins_url('/images/icon.png', __FILE__));
	add_action( 'admin_init', 'register_BAsettings' );
}
// ******************************************* ADMIN CUSTOM MENU START
// ******************************************* REGISTER CUSTOM SETTINGS START
function register_BAsettings() {
	//register our settings
	register_setting( 'bannerAuto-settings-group', 'selector_value');
	register_setting( 'bannerAuto-settings-group', 'selector_width' );
	register_setting( 'bannerAuto-settings-group', 'selector_height' );
	
}
// ******************************************* REGISTER CUSTOM SETTINGS END

// ******************************************* ADMIN CUSTOM SETTINGS PAGE START
function banner_autoload_settings_page() {
?>
<div class="wrap">
<h2>Autoload Settings</h2>
<form method="post" action="options.php">
    <?php 
		settings_fields( 'bannerAuto-settings-group' );
		do_settings_fields( 'bannerAuto-settings-group' , 'page');
		$default_selector = get_option('selector_value');
		$default_width = get_option('selector_width');
		$default_height = get_option('selector_height');
		
		if(empty($default_selector)):
			$default_selector = '#branding';
		else:			
			$default_selector = get_option('selector_value');
		endif;
		
		if(empty($default_width)):
			$default_width = '1000';
		else:			
			$default_width = get_option('selector_width');
		endif;
		
		if(empty($default_height)):
			$default_height = '400';
		else:			
			$default_height = get_option('selector_height');
		endif;
	?>
     <div>
        <!-- <label>Add your google analytics code (UA-XXXX-3) for the Advert tracking system.<label><br/> -->
		<p>Add the element selector #id or .class name below, that you wish to display the autoload banner to load into.<br/>
			   Please note that you must include the selector type with the selector's name value. <br/>
			   To use an ID the place the hash simbal # befor the selector name, if you are using a selector with a class name then place a dot . before the selector's name.<p><br/><br/>
        <label for="selector_value">Selector name: </label><input type="text" name="selector_value" id="selector_value" value="<?php echo $default_selector; ?>" /><br/><br/>
		<label for="selector_width">Banner width: </label>&nbsp;&nbsp;<input type="text" id="selector_width" name="selector_width" value="<?php echo $default_width; ?>" /><br/><br/>
		<label for="selector_height">Banner height: </label>&nbsp;&nbsp;<input type="text" id="selector_height" name="selector_height" value="<?php echo $default_height; ?>" />
      </div>

    <p class="submit">
    <input type="submit" class="button-primary" value="<?php _e('Save Settings') ?>" />
    </p>

</form>
</div>
<?php } 
// ******************************************* ADMIN CUSTOM SETTINGS PAGE END

function my_rewrite_flush() {
    // First, we "add" the custom post type via the above written function.
    // Note: "add" is written with quotes, as CPTs don't get added to the DB,
    // They are only referenced in the post_type column with a post entry, 
    // when you add a post of this CPT.
    banner_autoload_type();

    // ATTENTION: This is *only* done during plugin activation hook in this example!
    // You should *NEVER EVER* do this on every page load!!
    flush_rewrite_rules();
}
register_activation_hook( __FILE__, 'my_rewrite_flush' );


?>