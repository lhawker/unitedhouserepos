<?
/***************************************************************************************/
//	This is the banner autoload single template.
//	Banner autoload
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/


$default_width = get_option('selector_width');
$default_height = get_option('selector_height');


// print the JQ to the page for the carousel
echo '<script type="text/javascript">		
		jQuery(document).ready(function() {    		
			// initialize carousel
    		$(\'.carousel\').carousel({interval: 6000});
			$(\'.carousel\').carousel(\'next\');
			
			$(\'.large_banner_txt\').appendTo(\'#carousel-generic\');
		});
	   </script> ';
	
	//carousel-inner

?>


<?php 


global $wp_query, $post;
$selected_banner = get_post_meta($post->ID, 'selected_banner', true);
$selected_pages = get_post_meta($post->ID, 'selected_pages', true);

// Function to loopover the meta arrays
function meta_data_id_values($arrgs, $arrgs2, $arrgs3){
	// clean the array of unwanted keys & values	
	foreach( $arrgs as $key => $value ) {     	
		if (strpos($key, $arrgs2)) { 
         		unset($arrgs[$key]);
     	}		
		if (strpos($key, $arrgs3)) { 
         		unset($arrgs[$key]);
				//$arrgs['ID'] = $key;			
     	}
		if( empty( $arrgs[ $key ] ) )
        	unset( $arrgs[ $key ] );
		}
		return ($arrgs);
}
echo '<div class="banner-container">';
echo '<div id="carousel-generic" class="carousel slide">';
$i = 0;
$num = 0;
// nav items here please
echo '<ol class="carousel-indicators">';
	foreach (meta_data_id_values($selected_banner, 'NULL', 'NULL') as $key => $value):
		$i = $i + 1;
		if($i==1){
			$active_class = 'active';
		} else {
			$active_class = '';
		}
		echo '<li data-target="#carousel-generic" data-slide-to="'.$num.'" class="'.$active_class.'"></li>';
		$num = $num +1;
	endforeach;
echo '</ol>';

echo ' <div class="carousel-inner">';

foreach (meta_data_id_values($selected_banner, 'NULL', 'NULL') as $key => $value):
	$i = $i + 1;
	
	if($i==1){
			$active_class = 'active';
		} else {
			$active_class = '';
	}
	
	
	if (strpos($key, '_ID')):
		$banner_id = $value;
	else:
		$banner_id = NULL;
	endif;
	
	if(!$banner_id == NULL):
		$client = $banner_id.'_client';
		$banner_client = $selected_banner[$client];
		else:
		$banner_client = NULL;
	endif;
	
	if(!$banner_id == NULL):
		$url = $banner_id.'_url';
		$banner_url = $selected_banner[$url];		
	else:
		$banner_url = NULL;
	endif;
		
	if(!$banner_url == NULL):
		if(!$banner_id == NULL):
			echo '<div class="item '.$active_class.'">'.sprintf('<a href="http://%s?tcode=%s">%s</a>', $selected_banner[$url], $banner_client, preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>', wp_get_attachment_image($banner_id, 'large'))).'</div>';
		endif;
	else:
		if(!$banner_id == NULL):
		//	echo '<div class="item '.$active_class.'">'.preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)\>/i', '<$1$4$7>', wp_get_attachment_image($banner_id, 'large')).'</div>';
			echo '<div class="item '.$active_class.'">'.wp_get_attachment_image($banner_id, 'large').'</div>';

		endif;	
			
	endif;	
endforeach;
echo '</div>';
echo '<div class="left-mask"></div>';
echo '<div class="right-mask"></div>';
echo '<a class="left carousel-control" href="#carousel-generic" data-slide="prev">
          ‹
        </a>
        <a class="right carousel-control" href="#carousel-generic" data-slide="next">
          ›
        </a>';
echo '</div>';
echo '</div>'; // .container end
?>
