<?php
/*
	Plugin Name: Unitedhouse content types
	Plugin URI: http://www.studioeleven.uk.com
	Description: This is a plugin to add new content types to wordpress for the United house project.
	Version: 1.0
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
*/
/**************** Disable plugin deactivation for specific plugins */

/* ####################### INCLUDE CUSTOM POST TYPES */
###### 1. Projects content type include
include_once 'contentTypes/projects.php';
?>