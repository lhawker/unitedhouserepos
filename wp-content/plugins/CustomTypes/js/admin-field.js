jQuery(document).ready(function($) {	
   // change field (postexcerpt) title and description text

   $('#postexcerpt h3 span').text("Short Description");   
   $('#mappress .hndle span').text("Location Map");   
   
   
   // reorder the fields around
   $('#postdivrich').before($('#normal-sortables')); 
   //$('#titlediv').before($('#postexcerpt'));
   $('#mappress').before($('#postexcerpt'));
   $('.mapp-m-panel div:first').remove();
   
   $('#postdivrich').after($('#revisionsdiv'));
   $('#date_meta_fields').after($('#revisionsdiv')); 
   
   $('#postexcerpt .inside p').html('<em>Short descriptions are optional hand-crafted summaries of your content that can be used in page listings. </em>');
   
   
   var server_path = document.location.host,
   wordpress_path = location.pathname.split("/")[1];
      
  
   if ($('.datepiker').length != 0) {
    $(".datepiker").datetimepicker({
    		  showOn: 'button',   
          buttonImage: 'http://www.sineadoconnor.com/wp-content/themes/sineadoconnor_theme/custom_types/icons/tour_dates_icon.gif',         
          buttonImageOnly: true,
          dateFormat: 'd/m/yy',});
    } else {
    //alert('No i.datepiker elements found'); pass on witout errors
    }
   

});