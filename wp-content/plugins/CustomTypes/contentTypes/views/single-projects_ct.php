<?php
	// load header
	get_header();
	///echo do_shortcode('[build-project-category-menu]');
?>


<?php 

	$current_tax_name = get_the_terms($post->id, 'projects');

	if(!empty($current_tax_name)):
		$current_section_css_id = $current_tax_name[current(array_keys($current_tax_name))]->name;
		//strtolower($current_section_css_id);
	else:
		$current_section_css_id = site_url();
	endif;

	?>



<script type="text/javascript">
	// A $( document ).ready() block.
$( document ).ready(function() {
	//$('.breadcrumb').remove();
	//$("<div/>").replaceWith("<p></p>");
	$('.breadcrumb').children(':first-child').next().replaceWith('<li><a href="<?php echo site_url().'/'.strtolower($current_section_css_id);?>"><?php echo $current_section_css_id;?></a></li>');
<?php
				global $post;				
				$post_id = $post->ID;
				$post_id = '375';
				$post_type = $post->post_type;
				$post_type = 'projects_ct';
				$post_tax = 'projects';		
				$mapResults = new FindMaps();					

				$pieces = explode("]", str_replace('[{latLng:[', '', $mapResults->getMap($post_id, $post_type, $post_tax)));
				
			?>
	$('#projects_map').width('100%').height('350px').gmap3({
  		map:{
    		options:{
     			center:[<?php echo $pieces[0];?>],
     			zoom: 7,
				styles: [{featureType: 'all',stylers: [{ saturation: -90 },{ lightness: 50 }]}],
     			mapTypeControl: true,
				navigationControl: true,
     			scrollwheel: true,
     			streetViewControl: true
    		}
 		},

  		marker:{
    		values:<?php
				echo $mapResults->getMap($post_id, $post_type, $post_tax);	
			?>,
    
			options:{ draggable: false },

    		events:{
      			click: function(marker, event, context){
        			var map = $(this).gmap3('get'),
          			infowindow = $(this).gmap3({get:{name:'infowindow'}});
        			if (infowindow){
          				infowindow.open(map, marker);
          				infowindow.setContent(context.data);
        			} else {
          				$(this).gmap3({
            				infowindow:{
              					anchor:marker, 
              					options:{content: context.data}
            				}
          				});
        			}
      			},
      			
    		}
  		}
	});
});
$(function () {
    $('#projectTabs a').click(function (e) {
  		e.preventDefault();
  		$(this).tab('show');
	});
});
</script>
	
<div id="primary" class="content-area">
	<div id="content" class="site-content" role="main">
		<?php
		// load featured image
		if ( has_post_thumbnail() ) {
			$img_string = preg_replace('/\<(.*?)(width="(.*?)")(.*?)(height="(.*?)")(.*?)(class="(.*?)")(.*?)\>/i','<$1$4$7>', get_the_post_thumbnail());
			echo  '<div class="featured_image"><span class="projects_cluster_large_img">'.$img_string.'</span></div>';
		} 
		?>
		<section>
				<?php
				
				if (function_exists('synved_social_follow_markup')){
					echo do_shortcode('[feather_share]');
				} 
				
				?>
			<header class="entry-header">
				<h2 class="entry-title"><?php the_title(); ?></h2>
			</header><!-- .entry-header -->
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
				

				
						
				<?php echo get_the_content(); ?>
				
			<?php endwhile; ?>
			</article>
			<div class="projects_back_btn">
				<a class="btn btn-info" href="/projects" title="Back to Projects">&laquo; Back to Projects</a>
			</div>
			<br/>
		</section>
		<section id="downloads">
			<?php
					if (function_exists('userTypeAttachments')) {
						userTypeAttachments();
					}
				?>
		</section>
		<section>
			<!-- map and ajax -->
			<div class="map_area">
				<div id="projects_map">map pleases</div>
				<!-- <div class="map_key">
					<ul>
						<li>Map Key:</li>
						<li class="developments_map_icon"><span>&nbsp;</span>Developments</li>
						<li class="refurbishment_map_icon"><span>&nbsp;</span>Refurbishment</li>
						<li class="contractor_map_icon"><span>&nbsp;</span>Contractor</li>
					</ul>
				</div> -->
			</div>			
			<br/>
		</section>
		<section id="gallery">			
			<?php $gallery = get_post_meta($post->ID, 'select_gallery', true);
				  if(!empty($gallery)):
					$gal = $gallery;
					echo '<h2>Gallery</h2>';
				  	echo do_shortcode('[nggallery id='.$gal.']');
				  endif;
				  
			?>
		</section>
	</div>
</div>

<?php
	
	
	// load footer
	get_footer();
	
?>

