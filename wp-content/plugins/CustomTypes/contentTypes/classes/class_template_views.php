<?php
class Template_views {
	
	public function __construct(){
		add_filter('init', array(&$this, 'init'));
	}
	
	public function locate_plugin_template($template_names, $load = false, $require_once = true ) {
	
    	if ( !is_array($template_names) )
        	return '';
			$located = '';
    		$this_plugin_dir = WP_PLUGIN_DIR.'/'.str_replace( basename( __FILE__), "", str_replace('/classes/', '/views/', 	plugin_basename(__FILE__)) ); 

			foreach ( $template_names as $template_name ) {
        		if ( !$template_name )
            		continue;
        			if ( file_exists(TEMPLATEPATH . '/' . $template_name) ) {
            			$located = TEMPLATEPATH . '/' . $template_name;
            			break;
        			} else if ( file_exists( $this_plugin_dir .  $template_name) ) {
            			$located =  $this_plugin_dir . $template_name;
            			break;
        			}
    			}    
    	if ( $load && '' != $located )
        	load_template( $located, $require_once );
    	return $located;
	}
/**************** Function for single template *************/

	public function get_custom_single_template($template) {
		global $wp_query;
    	$object = $wp_query->get_queried_object();
    
    	if ( 'projects_ct' == $object->post_type ) {
        	$templates = array('single-' . $object->post_type . '.php', 'single.php');
			$template = $this->locate_plugin_template($templates);	
    	}
    	// return apply_filters('single_template', $template);
    	return $template;
	}

	
	public function init(){
		add_filter('single_template', array(&$this, 'get_custom_single_template'));

	}
}
?>