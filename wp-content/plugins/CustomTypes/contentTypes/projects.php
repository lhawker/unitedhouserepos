<?php 
/***************************************************************************************
	Description: This is the projects content type model
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
/***************************************************************************************/

// ******************************************* POST TYPES FIELD INCLUDES START

// load the Ticket url field
include_once 'fields/gallery_select_field.php';
include_once 'fields/date_picker_field.php';
include_once 'fields/group_attachment_field.php';
include_once 'classes/class_template_views.php';
include_once 'classes/class_short_codes.php';



// ******************************************* POST TYPES FIELD INCLUDES START

// ******************************************* POST TYPES START
// Post Type Registration (Projects page-type).

function projects_CT() {
	$labels = array(
    	'name' => _x('Project', 'post type general name'),
    	'singular_name' => _x('Project', 'post type singular name'),
    	'add_new' => _x('Add New Project', 'Project'),
    	'add_new_item' => __('Add Project'),
    	'edit_item' => __('Edit Project'),
    	'new_item' => __('New Project'),
    	'view_item' => __('View Project'),
    	'search_items' => __('Search Projects'),
    	'not_found' =>  __('No Project found'),
    	'not_found_in_trash' => __('No Projects found in Trash'),
    	'parent_item_colon' => '',
	);
 	$projects_CT_args = array(
     	'labels' => $labels,
     	'singular_label' => __('Project'),
     	'public' => true,
     	'can_export' => true,
    	'show_ui' => true,
		'show_in_menu' => 'projects',
		'exclude_from_search' => false,
     	'capability_type' => 'page',
     	'hierarchical' => false,
     	'rewrite' => true,
		'has_archive' => true,
     	'supports' => array('title', 'excerpt', 'thumbnail','editor', 'revisions'),
		'taxonomies' => array('projects', 'projects_tags')
     );
 	register_post_type('projects_CT',$projects_CT_args);
}
add_action('init', 'projects_CT');


// ******************************************* POST TYPES END
// ******************************************* Registration of Tour Dates Taxonomy START
// 1. Custom Taxonomy Registration (Tour Dates Type)

function create_projects_taxonomy() {
    $categories_labels = array(
        'name' => _x( 'Development Categories', 'taxonomy general name' ),
        'singular_name' => _x( 'Development Category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Development Categories' ),
        'popular_items' => __( 'Popular Development Categories' ),
        'all_items' => __( 'All Development Categories' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Development Category' ),
        'update_item' => __( 'Update Development Category' ),
        'add_new_item' => __( 'Add New Development Category' ),
        'new_item_name' => __( 'New Development Category Name' ),
        'separate_items_with_commas' => __( 'Separate Development Categories names with commas' ),
        'add_or_remove_items' => __( 'Add or remove Development Categories' ),
        'choose_from_most_used' => __( 'Choose from the most used Development Categories' ),
    );

	$tag_labels = array(
        'name' => _x( 'Development Status', 'taxonomy general name' ),
        'singular_name' => _x( 'Development Status', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Development Status' ),
        'popular_items' => __( 'Popular Development Status' ),
        'all_items' => __( 'All Development Status' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Edit Development Status' ),
        'update_item' => __( 'Update Development Status' ),
        'add_new_item' => __( 'Add New Development Status' ),
        'new_item_name' => __( 'New Development Status Name' ),
        'separate_items_with_commas' => __( 'Separate Development Status names with commas' ),
        'add_or_remove_items' => __( 'Add or remove Development Status' ),
        'choose_from_most_used' => __( 'Choose from the most used Development Status' ),
    );

    register_taxonomy('projects','categories', array(
        'label' => __('Categories'),
        'labels' => $categories_labels,
        'hierarchical' => true,
        'show_ui' => true,
		'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'projects' ),
    ));

	register_taxonomy('projects_tags','categories', array(
        'label' => __('Project Status Term'),
        'labels' => $tag_labels,
        'hierarchical' => false,
        'show_ui' => true,
		'show_in_nav_menus' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'projects_tags' ),
    ));

}

add_action( 'init', 'create_projects_taxonomy', 0 );
// ******************************************* Registration of Tour Dates Taxonomy END


// ******************************************* ADMIN CUSTOM MENU START
function projects_area_create_admin_menu() {
	global $current_user;

	if ($current_user->roles[0] == 'hr_team' ){
		
	} else {
		add_menu_page('Projects', 'Projects', 'publish_pages', 'projects', '',str_replace('contentTypes', 'icons', plugins_url( 'projects_icon.png' , __FILE__ )),14,'');
	add_submenu_page( 'projects', 'Developments', 'Developments', 'manage_categories','edit-tags.php?taxonomy=projects');
		add_submenu_page( 'projects', 'Project Status Term', 'Project Status Term', 'manage_categories','edit-tags.php?taxonomy=projects_tags');
	}
}

add_action('admin_menu', 'projects_area_create_admin_menu');

/* highlight the proper top level menu */ /*working */
function projects_tax_menu_correction($parent_file) {
	global $current_screen;
	
		if (isset($current_screen->taxonomy)):
			$taxonomy = $current_screen->taxonomy;
			if ($taxonomy == 'projects' || $taxonomy == 'projects' && $taxonomy == 'projects_tags' || $taxonomy == 'projects_tags')
		$parent_file = 'projects';
		return $parent_file;
	endif;	

}
add_action('parent_file', 'projects_tax_menu_correction'); 
// ******************************************* ADMIN CUSTOM MENU START

// ******************************************* ADMIN CUSTOM JS START
function loadProjectJavaScripts(){
	 wp_enqueue_script('custom_script', trailingslashit(WP_PLUGIN_URL) . 'CustomTypes/js/admin-field.js', array('jquery'));

}
add_action( 'admin_print_scripts-post.php', 'loadProjectJavaScripts', 1000 );
add_action( 'admin_print_scripts-post-new.php', 'loadProjectJavaScripts', 1000 );
// ******************************************* ADMIN CUSTOM JS END

/**************** Function to be able to locate templates from within the plugin directory *************/

$project_template_view = new Template_views();


$shortcode_obj = new ShortCodes();
$shortcode_obj->registerShortCodes();
//echo $shortcode_obj->s1();




?>