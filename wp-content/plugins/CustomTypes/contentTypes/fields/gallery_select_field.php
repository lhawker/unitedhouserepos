<?php
/***************************************************************************************
	Description: Custom galley select field. Used to find all galleries within the database
	and select one to use with the selected content type.
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
/***************************************************************************************/

	
	// ******************************************* Add Custom META-BOX START
	add_action( 'admin_init', 'gallery_id_metabox' );

	function gallery_id_metabox() {
		$priority = 'high';
		add_meta_box('gallery_meta_fields', 'Photo Gallery', 'gallery_meta_fields', 'projects_ct', 'normal', 'core');	
	}
	
	// ******************************************* Add Custom META-BOX END
	
	// ******************************************* Add Custom Fields START
	function gallery_meta_fields () {
		global $wpdb, $post;
		
		$getPostId = get_post_custom($post->ID);
		
		if(!empty($getPostId)){
			$metaValue = $getPostId["select_gallery"][0];
		} else {
			$metaValue = null;
		}
		
		// include select field class
		include_once 'classes/class_select_field.php';
	
		// query the database to find all galley items.
		$queryPosts ="SELECT * FROM `{$wpdb->prefix}ngg_gallery` ORDER BY gid";
		$data = $wpdb->get_results($queryPosts);
		// Create new select field object
		$select_field_obj = new SelectField('gid', 'title', 'Select the gallery to display for this page: ','select_gallery', 120, $data, $metaValue);
				
		$select_field_obj;
	}
	// ******************************************* Add Custom Fields END
	
	// ******************************************* Save Fields Meta data START
	// Save the Data
	add_action ('save_post', 'save_gallery_meta_fields');
	function save_gallery_meta_fields(){
		global $post;
		if (isset($_POST['select_gallery'])) {
		// Update start time
			update_post_meta($post->ID, "select_gallery", $_POST["select_gallery"] );	
		
		if ( !current_user_can( 'edit_post', $post->ID ))
        	return $post->ID;
		}
	}
	// ******************************************* Save Fields Meta data END
	
?>