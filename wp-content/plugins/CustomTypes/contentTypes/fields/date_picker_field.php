<?php
/***************************************************************************************/
//	This loads the date picker javascript and css to the admin screen
//	Author: Geansai .Ltd
//	Author URI: http://geansai.co.uk
//	Copyright (C) 2011-2011 Geansai .Ltd / geansai.co.uk (info@geansai.co.uk)
/***************************************************************************************/


// define WP_THEME_URL
if(!defined('WP_THEME_URL')):
	define( 'WP_THEME_URL', get_bloginfo('stylesheet_directory'));
endif;


/***************************************************************************************/
// 1. Add Custom Meta-Boxs
add_action( 'admin_init', 'dates_create_metabox' );

	function dates_create_metabox() {
		$priority = 'high';
		add_meta_box('date_meta_fields', 'Project Dates', 'date_meta_fields', 'projects_ct');	
	}

// include select field class
	$post_types_array = array('projects_ct', 'news_ct');
	include_once 'classes/class_date_field.php';
	new RegisterStartEndDateField('projects_ct',$post_types_array);


// 1. Start tour date field
function date_meta_fields () {
    // - grab data -
    global $post;
	$dateTimeFormat = 'd/m/Y H:i';
    $custom = get_post_custom($post->ID);

	if (isset($custom["start_date"][0])):
		$meta_timestap = $custom["start_date"][0];
	else:
		$meta_timestap = date($dateTimeFormat);
	endif;
	
	if (isset($custom["end_date"][0])):
		$meta_end_timestap = $custom["end_date"][0];
	else:
		$meta_end_timestap = date($dateTimeFormat);
	endif;
	
	new StartEndDateField($meta_timestap, $meta_end_timestap);
}

// ******************************************* Save Fields Meta data START
// Save the Data
add_action ('save_post', 'save_date_meta_fields');

// SAVE function for the tour dates post type
function save_date_meta_fields(){
    global $post;

	if (isset($_POST['start_date'])) {
		// Update start time
        update_post_meta($post->ID, "start_date", $_POST["start_date"] );
		if ( !current_user_can( 'edit_post', $post->ID ))
        	return $post->ID;
	}
	if (isset($_POST['end_date'])) {
		// Update the end time
        update_post_meta($post->ID, "end_date", $_POST["end_date"] );
		if ( !current_user_can( 'edit_post', $post->ID ))
        	return $post->ID;
	}     
	
		
}
// ******************************************* Save Fields Meta data END

// Customize Update Messages
add_filter('post_updated_messages', 'tourdates_updated_messages');
// Courses update message function
function tourdates_updated_messages( $messages ) {
  global $post, $post_ID;
  $messages['tour_dates'] = array(
    0 => '', // Unused. Messages start at index 1.
    1 => sprintf( __('Project dates updated. <a href="%s">View item</a>'), esc_url( get_permalink($post_ID) ) ),
    2 => __('Custom field updated.'),
    3 => __('Custom field deleted.'),
    4 => __('Project dates updated.'),
    /* translators: %s: date and time of the revision */
    5 => isset($_GET['revision']) ? sprintf( __('Project dates restored to revision from %s'), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
    6 => sprintf( __('Project dates published. <a href="%s">View Project date</a>'), esc_url( get_permalink($post_ID) ) ),
    7 => __('Project dates saved.'),
    8 => sprintf( __('Project dates submitted. <a target="_blank" href="%s">Preview Project date</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
    9 => sprintf( __('Project dates scheduled for: <strong>%1$s</strong>. <a target="_blank" href="%2$s">Preview Project date</a>'),
      // translators: Publish box date format, see http://php.net/date
      date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ), esc_url( get_permalink($post_ID) ) ),
    10 => sprintf( __('Project dates draft updated. <a target="_blank" href="%s">Preview Project date</a>'), esc_url( add_query_arg( 'preview', 'true', get_permalink($post_ID) ) ) ),
  );
  return $messages;
}


?>