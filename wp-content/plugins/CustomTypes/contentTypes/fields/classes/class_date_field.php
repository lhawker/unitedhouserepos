<?php
/***************************************************************************************
	Description: Class to render a start and end date field
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
/***************************************************************************************/
class RegisterStartEndDateField {
	
	private $PostType, $PostTypesArray;
	
	public function __construct($post_type, $post_type_array){
		$this->PostTypesArray = $post_type_array;
		$this->PostType = $post_type;
		add_action('init', array(&$this, 'init'));
	}

	// load styles for the admin edit page
	public function datePickerStyles() {	
		if (!in_array($this->PostType,$this->PostTypesArray))
        	return;
    		wp_enqueue_style('ui-datepicker', trailingslashit(WP_PLUGIN_URL) .
 			'CustomTypes/css/jquery-ui-1.8.9.customd.css');
	}
	
	
	public function datePickerScripts(){
		if (!in_array($this->PostType,$this->PostTypesArray))
        	return;
			wp_enqueue_script('jquery-ui', trailingslashit(WP_PLUGIN_URL) .
			'CustomTypes/js/jquery-ui-1.9.2.custom.min.js', array('jquery'));
			wp_enqueue_script('ui-datepicker', trailingslashit(WP_PLUGIN_URL) .
			'CustomTypes/js/jquery.ui.datepicker.js', array('jquery'));
			wp_enqueue_script('ui-timedatepicker', trailingslashit(WP_PLUGIN_URL) .
			'CustomTypes/js/jquery-ui-timepicker-addon.js', array('jquery'));

	}
	
	
	
	public function init(){
		add_action('admin_print_styles-post.php', array(&$this, 'datePickerStyles'), 1000);
		add_action('admin_print_styles-edit.php', array(&$this, 'datePickerStyles'), 1000);
		add_action('admin_print_styles-edit-tags.php', array(&$this, 'datePickerStyles'), 1000);
		add_action('admin_print_styles-post-new.php', array(&$this, 'datePickerStyles'), 1000);
		add_action('admin_print_scripts-post.php', array(&$this, 'datePickerScripts'), 1000);
		add_action('admin_print_scripts-post-new.php', array(&$this, 'datePickerScripts'), 1000);
	}
}


class StartEndDateField {
	private $StartDateTime, $EndDateTime;
	
	public function __construct($start_timestap ,$end_timestap){
		$this->StartDateTime = $start_timestap;
		$this->EndDateTime = $end_timestap;
		$this->date_meta_fields();
	}
	
	public function date_meta_fields() {
		echo 
		'<div class="date-meta">
        	<ul>  
				<li>
				<label for="start_date">Start date: </label>
				<input class="datepiker" type="text" id="start_date" name="start_date" value="'.$this->StartDateTime.'"  />					
				<span>&nbsp;</span>
				<label for="end_date">End date: </label>
				<input class="datepiker" type="text" id="end_date" name="end_date" value="'.$this->EndDateTime.'"  />
				</li> 				
        	</ul>
    	</div>';
	}
}
?>