<?php
/***************************************************************************************
	Description: Class to render a select field
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
/***************************************************************************************/

class SelectField {
	private $DBColumnID, $DBColumnTitile, $FieldLabel, $FieldID, $FieldSize, $FieldData, $FieldDataMetaValue;
    
	public function __construct($bd_id_column, $bd_title_column, $label, $id, $size, $data, $metaValue) {
	  //echo $FieldID;
	$this->DBColumnID = $bd_id_column;
	$this->DBColumnTitile = $bd_title_column;
	
	
	$this->FieldLabel = $label;
	$this->FieldID = $id;
	$this->FieldSize = $size;
	$this->FieldData = $data;
	$this->FieldDataMetaValue = $metaValue;
	$this->renderField();

   }

	function renderField(){
		$lable_tag = '<label for="'.$this->FieldID.'">'.$this->FieldLabel.'</label>';	
		$select_tag_open = '<select name="'.$this->FieldID.'" id="'.$this->FieldID.'">';
		$option_tag_no_selection = '<option value="0">Select a Gallery</option>';
		$select_tag_closed = '</select>';
		
		$optionData = null;
		
		foreach($this->FieldData as $item) {
			$id = $this->DBColumnID;
			$title = $this->DBColumnTitile;
			$matchMetaValue = $this->FieldDataMetaValue;

			if($matchMetaValue == $item->$id) {
				$optionData .= $item->$id;
				$optionData .= '<option value="'.$item->$id.'" selected>'.$item->$title.'</option>';
			} else {
				$optionData .= $item->$id;
				$optionData .= '<option value="'.$item->$id.'">'.$item->$title.'</option>';	
			}
			
		}
		echo $lable_tag . $select_tag_open . $option_tag_no_selection . $optionData . $select_tag_closed;
    }

	

}
?>