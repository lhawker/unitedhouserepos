<?php
/***************************************************************************************
	Description: Custom galley select field. Used to find all galleries within the database
	and select one to use with the selected content type.
	Author: Studio Eleven
	Author URI: http://www.www.studioeleven.uk.com
	Copyright (C) 2013 Studio Eleven / info@studioeleven.uk.com
/***************************************************************************************/

// ******************************************* Add Custom Fields START
/**
 * Adding our custom fields to the $form_fields array
 * 
 * @param array $form_fields
 * @param object $post
 * @return array
 */
function attachment_group_selectbox($form_fields, $post) {
	$post_mime_type = array('application/pdf','application/zip', 'text/plain', 'application/msword', 'application/vnd.ms-excel');
	
	// echo $post->post_mime_type; // show the files mind type 
	if(in_array($post->post_mime_type, $post_mime_type, true)) { 
		
		// select options: you could code these manually or get it from a database
		$select_options = array(
			"developer" => "Developer (Group-Role)",
			"contractor" => "Contractor (Group-Role)",
			"refurbisher" => "Refurbisher (Group-Role)",
		);

		// get the current value of our custom field
		$current_value = get_post_meta($post->ID, "_attachmentGroup", true);

	
		// build the html for our select box
		$mySelectBoxHtml = "<select multiple name='attachments[{$post->ID}][attachmentGroup][]' id='attachments[{$post->ID}][attachmentGroup]'>";
	
		foreach($select_options as $value => $text){
			$selected = '';
			if(is_array($current_value)){
				if (in_array($value, $current_value, true)) {
					$selected = ' selected ';
				} 
			}

			$value = addcslashes( $value, "'");		
			$mySelectBoxHtml .= "<option value='{$value}' {$selected}>{$text}</option>";
		}
		
		$mySelectBoxHtml .= "</select>";
	
		// add our custom select box to the form_fields
		$form_fields["attachmentGroup"]["label"] = __("Attachment Group: ");
		$form_fields["attachmentGroup"]["input"] = "html";
		$form_fields["attachmentGroup"]["html"] = $mySelectBoxHtml;
		$form_fields["attachmentGroup"]["helps"] = "Select the group who can view this attachment from the above list.<br/>Hold down the Ctrl (windows) / Command (Mac) button to select multiple options.";
	}

	return $form_fields;
}
add_filter("attachment_fields_to_edit", "attachment_group_selectbox", null, 2);
// ******************************************* Add Custom Fields END
// ******************************************* Save Fields Meta data START
function attachment_group_selectbox_save($post, $attachment) {
	if( isset($attachment['attachmentGroup']) ){
		update_post_meta($post['ID'], '_attachmentGroup', $attachment['attachmentGroup']);
	}
	return $post;
}
add_filter("attachment_fields_to_save", "attachment_group_selectbox_save", null, 2);
// ******************************************* Save Fields Meta data END
	
?>